<?php include("includes/head.php"); ?>
<?php include("includes/header.php"); ?>

        <div class="wrapper">

            <div class="section">
                <div class="test">
                    <h1 class="hdg-1 breakpointTester">Hello World, you are in the breakpoint: </h1>
                </div>
            </div>

            <div class="section">
                <div class="test">
                    <div class="hdg-1">Blocks Objects</div>
                </div>
                <div class="grid">
                    <div class="grid-col grid-col_6">
                        Blocks 5 Up
                        <ul class="blocks blocks_5up">
                            <li>One</li>
                            <li>Two</li>
                            <li>Three</li>
                            <li>Four</li>
                            <li>Five</li>
                        </ul>
                    </div>
                    <div class="grid-col grid-col_6">
                        Blocks 2 Up
                        <ul class="blocks blocks_2up">
                            <li>One</li>
                            <li>Two</li>
                        </ul>
                    </div>
                </div>
                <div class="grid">
                    <div class="grid-col grid-col_3">
                        Blocks 2 Up
                        <ul class="blocks blocks_2up">
                            <li>One</li>
                            <li>Two</li>
                        </ul>
                    </div>
                    <div class="grid-col grid-col_4">
                        Blocks 3 Up
                        <ul class="blocks blocks_3up">
                            <li>One</li>
                            <li>Two</li>
                            <li>Three</li>
                        </ul>
                    </div>
                    <div class="grid-col grid-col_5">
                        Blocks 4 Up
                        <ul class="blocks blocks_4up">
                            <li>One</li>
                            <li>Two</li>
                            <li>Three</li>
                            <li>Four</li>
                        </ul>
                    </div>
                </div>
                <div class="grid">
                    <div class="grid-col grid-col_6">
                        Blocks Flush 5 Up
                        <ul class="blocks blocks_flush blocks_5up">
                            <li>One</li>
                            <li>Two</li>
                            <li>Three</li>
                            <li>Four</li>
                            <li>Five</li>
                        </ul>
                    </div>
                    <div class="grid-col grid-col_6">
                        Blocks Flush 2 Up
                        <ul class="blocks blocks_flush blocks_2up">
                            <li>One</li>
                            <li>Two</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="section">
                <div class="test">
                    <div class="hdg-1">Grid System: 12 Column</div>
                </div>
                <div class="grid">
                    <div class="grid-col grid-col_2">2</div>
                    <div class="grid-col grid-col_2 grid-col_push2">2 - push2</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_3 grid-col_push2">3 - push2</div>
                </div>
                <div class="grid">
                    <div class="grid-col grid-col_6 grid-col_push5">6 - push5</div>
                    <div class="grid-col grid-col_1">1</div>
                </div>
                <div class="grid">
                    <div class="grid-col grid-col_4">4</div>
                    <div class="grid-col grid-col_4 grid-col_push4">4 - push4</div>
                </div>
                <div class="grid">
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                </div>
                <div class="grid">
                    <div class="grid-col grid-col_2">2</div>
                    <div class="grid-col grid-col_2">2</div>
                    <div class="grid-col grid-col_2">2</div>
                    <div class="grid-col grid-col_2">2</div>
                    <div class="grid-col grid-col_2">2</div>
                    <div class="grid-col grid-col_2">2</div>
                </div>
                <div class="grid">
                    <div class="grid-col grid-col_3">3</div>
                    <div class="grid-col grid-col_3">3</div>
                    <div class="grid-col grid-col_3">3</div>
                    <div class="grid-col grid-col_3">3</div>
                </div>
                <div class="grid">
                    <div class="grid-col grid-col_4">4</div>
                    <div class="grid-col grid-col_4">4</div>
                    <div class="grid-col grid-col_4">4</div>
                </div>
                <div class="grid">
                    <div class="grid-col grid-col_5">5</div>
                    <div class="grid-col grid-col_2">2</div>
                    <div class="grid-col grid-col_5">5</div>
                </div>
                <div class="grid">
                    <div class="grid-col grid-col_6">6</div>
                    <div class="grid-col grid-col_6">6</div>
                </div>
            </div>

            <div class="section">
                <div class="test">
                    <div class="hdg-1">Grid System: 12 Column Flush</div>
                </div>
                <div class="grid grid_flush">
                    <div class="grid-col grid-col_2">2</div>
                    <div class="grid-col grid-col_2 grid-col_push2">2 - push2</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_3 grid-col_push2">3 - push2</div>
                </div>
                <div class="grid grid_flush">
                    <div class="grid-col grid-col_6 grid-col_push5">6 - push5</div>
                    <div class="grid-col grid-col_1">1</div>
                </div>
                <div class="grid grid_flush">
                    <div class="grid-col grid-col_4">4</div>
                    <div class="grid-col grid-col_4 grid-col_push4">4 - push4</div>
                </div>
                <div class="grid grid_flush">
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                </div>
                <div class="grid grid_flush">
                    <div class="grid-col grid-col_2">2</div>
                    <div class="grid-col grid-col_2">2</div>
                    <div class="grid-col grid-col_2">2</div>
                    <div class="grid-col grid-col_2">2</div>
                    <div class="grid-col grid-col_2">2</div>
                    <div class="grid-col grid-col_2">2</div>
                </div>
                <div class="grid grid_flush">
                    <div class="grid-col grid-col_3">3</div>
                    <div class="grid-col grid-col_3">3</div>
                    <div class="grid-col grid-col_3">3</div>
                    <div class="grid-col grid-col_3">3</div>
                </div>
                <div class="grid grid_flush">
                    <div class="grid-col grid-col_4">4</div>
                    <div class="grid-col grid-col_4">4</div>
                    <div class="grid-col grid-col_4">4</div>
                </div>
                <div class="grid grid_flush">
                    <div class="grid-col grid-col_5">5</div>
                    <div class="grid-col grid-col_2">2</div>
                    <div class="grid-col grid-col_5">5</div>
                </div>
            </div>

        </div> <!-- END .wrapper -->
        

    <?php include("includes/footer.php"); ?>

    </body>
</html>
