<?php include("includes/head.php"); ?>
<?php include("includes/header.php"); ?>
        
            <div class="wrapperFull">
                <div class="introSection js-introSection">
                    <figure class="introSection-background">
                        <img class="introSection-background_img" src="assets/images/maters.jpg" />
                        <img class="introSection-background_img" src="assets/images/curts-special-recipe.jpg" alt="Empty Salsa Jars" />
                        <img class="introSection-background_img" src="assets/images/curts-fresh-ingredients.jpg" />   
                    </figure>
                    <div class="wrapper wrapper_cover introSection-textBlock">
                        <h2>
                            <span class="hdg mix-txt_intro">Ordinary Label.</span>
                            <span class="hdg hdg_huge">Extraordinary</span>
                            <span class="hdg hdg_lrg">Ingredients.</span>
                        </h2>
                        <p class="bdcpy bdcpy_lrg mix-txt_light">For nearly 20 years we've been making salsa and now BBQ sauce and Bloody Mary Mix, the way it’s meant to be made: in small batches, using the best ingredients we can find, and of course, Curt's original signature blend of herbs and spices.</p>
                        <a class="btn btn_std btn_light" href="shop.php">Go Buy Some!</a>
                    </div>
                </div>
            </div>

            <section id="js-slideInOnScroll" class="slideInOnScrollWrapper">
                <section class="wrapperFull wrapperFull_light">
                    <div class="wrapper">
                        <div class="media slideInGroup">
                            <article class="media-bd grid-col grid-col_7">
                                <div class="slideInObject slideInObject_left">
                                    <h3 class="hdg hdg_1 mix-txt_dark">It all started back in 1994. Turning a country cabin into a chef's kitchen.</h3>
                                    <p class="bdcpy">Out in the Wisconsin woods, Curt and Betty Hollister, two self-described foodies, perfected their savory blend of tomatoes, onions and peppers and began selling their homemade salsa at local farmer’s markets, under the eponymous name Curt's “Special Recipe” Salsa.</p>
                                </div>
                            </article>
                            
                            <figure class="media-media grid-col grid-col_4">
                                <div class="slideInObject slideInObject_right">
                                    <img class="img " src="assets/images/salsa-jars.png" alt="Empty Salsa Jars" />                                 
                                </div>
                            </figure>
                        </div>
                    </div>
                </section>

                <section class="wrapperFull wrapperFull_dark">
                    <div class="media media_flip slideInGroup wrapper">
                        <article class="media-bd grid-col grid-col_7">
                            <div class="slideInObject slideInObject_right">
                                <h3 class="hdg hdg_1">One million jars later, Curt’s Special Recipe is still made the way it has always been made.</h3>
                                <p class="bdcpy">The Hollisters are no longer in the kitchen, but their handcrafted, small-batch way of doing things continues to be the bedrock of how we do things. And now, we're happy to introduce Curt's Special Recipe<sup>TM</sup> BBQ sauce and Bloody Mary mix.</p>
                            </div>
                        </article>
                        
                        <figure class="media-media grid-col grid-col_4">
                            <div class="slideInObject slideInObject_left">
                                <img class="img " src="assets/images/freshness.png" alt="Empty Salsa Jars" />                                 
                            </div>
                        </figure>
                    </div>
                </section>

                <section class="wrapperFull wrapperFull_light">
                    <div class="wrapper">
                        <div class="media slideInGroup">
                            <article class="media-bd grid-col grid-col_7">
                                <div class="slideInObject slideInObject_left">
                                    <h3 class="hdg hdg_1 mix-txt_dark">Don't let the plain white label fool you.</h3>
                                    <p class="bdcpy">Curt's Special Recipe<sup>TM</sup> isn't your typical mass-produced, bland, or watered-down sauce company. No sir. Behind these plain white labels are some of the freshest, most flavorful sauces you'll find anywhere, from tangy-mild, to knee-buckling hot, to bold, rich and smoky, we have a flavor profile sure to fit any sauce-lovers palate.</p>
                                </div>
                            </article>
                            
                            <figure class="media-media grid-col grid-col_4">
                                <div class="slideInObject slideInObject_right">
                                    <img class="img" src="assets/images/plain-label.png" alt="Empty Salsa Jars" />
                                </div>
                            </figure>
                        </div>
                    </div>
                </section>
            </section>

            <section class="slider">
                <div class="flexslider flexslider_sm js-flexslider_sm carousel">
                    <!-- Images demensions should be around 455x300 -->
                    <ul class="slides">
                        <li><img src="assets/images/slider-sm/jarandpeppers.jpg" alt=" "></li>
                        <li><img src="assets/images/slider-sm/peppers.jpg" alt=" "></li>
                        <li><img src="assets/images/gallery/fresh-salsa-ingredients.jpg" alt=" "></li>
                        <li><img src="assets/images/slider-sm/tomatoes-clams.jpg" alt=" "></li>
                        <li><img src="assets/images/slider-sm/peppers-and-tomatoes.jpg" alt=" "></li>
                        <li><img src="assets/images/slider-sm/bloody-mary-mix.jpg" alt=" "></li>
                        <li><img src="assets/images/slider-sm/ingredients.jpg" alt=" "></li>
                        <li><img src="assets/images/slider-sm/spicy-barbeque.jpg" alt=" "></li>
                        <li><img src="assets/images/gallery/fresh-tomatoes.jpg" alt=" "></li>
                        <li><img src="assets/images/slider-sm/vegetables.jpg" alt=" "></li>
                        <li><img src="assets/images/gallery/spices.jpg" alt=" "></li>             
                        <li><img src="assets/images/gallery/fresh-salsa-ingredients-2.jpg" alt=" "></li>
                        
                    </ul>
                </div>
            </section>

            <section class="wrapperFull wrapperFull_wood wrapperFull_spread">
                <div class="wrapper txtCenter">
                    <h3 class="hdg hdg_1">Some of the nice things people have to say about us:</h3>
                    <ul class="hList publicity">
                        <li class="btn btn_sm btn_light"><a>Heavy Table</a></li>
                        <li class="btn btn_lrg btn_light"><a>Fieryfoods.com</a></li>
                        <li class="btn btn_sm btn_light"><a>Msp Magazine</a></li>
                        <li class="btn btn_lrg btn_light"><a>Media Times Messenger</a></li>
                        <li class="btn btn_sm btn_light"><a>Hot Sauce Blog</a></li>
                        <li class="btn btn_sm btn_light"><a>Chow Hound</a></li>
                        <li class="btn btn_sm btn_light"><a>City Pages</a></li>
                        <li class="btn btn_lrg btn_light"><a>Mpls/ St. Paul Business Journal</a></li>
                        <li class="btn btn_sm btn_light"><a>Scovie Awards</a></li>
                    </ul>
                    <p class="hdg hdg_2">Would You Like to Write About Curt’s? <a class="textLink mix-txt_underline">Email Us.</a></p>
                </div>
            </section>


            <section class="wrapperFull wrapperFull_spread wrapperFull_light txtCenter">
                <h4 class="hdg hdg_1 mix-txt_dark">What are you waiting for? Go grab a jar or two!</h4>
                <a class="btn btn_std btn_dark center" href="shop.php">GO BUY SOME!</a>
            </section>


        <?php include("includes/footer.php"); ?>

    </body>
</html>
