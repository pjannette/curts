<?php include("includes/head.php"); ?>
<?php include("includes/header.php"); ?>

        <section class="wrapperFull wrapperFull_wood wrapperFull_spread">
            <div class="wrapper">
                <div class="equalHeightCols">
                    <div class="equalHeightCols-col buyLinks">
                        <h2 class="hdg hdg_1">Find us in these fine establishments</h2>
                        <a><i class="icon icon_light icon_buyLink icon-cub"></i></a>
                        <a><i class="icon icon_light icon_buyLink icon-festival"></i></a>
                        <a><i class="icon icon_light icon_buyLink icon-target"></i></a>
                        <a><i class="icon icon_light icon_buyLink icon-lunds"></i></a>
                        <a><i class="icon icon_light icon_buyLink icon-byerlys"></i></a>
                        <a><i class="icon icon_light icon_buyLink icon_buyLink_sm icon-kowalskis"></i></a>
                        <a><i class="icon icon_light icon_buyLink icon_buyLink_sm2 icon-hornbachers"></i></a>
                        <p class="bdcpy em">Your Company Here</p>                 
                    </div>
                    <div class="equalHeightCols-col equalHeightCols-col_dark">
                        <div class="locationFinder">
                            <h2 class="hdg hdg_1">Find a Location</h2>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="wrapperFull wrapperFull_white wrapperFull_spread">
            <div class="wrapper">
                <h2 class="hdg hdg_1 mix-txt_dark">More fine establishments carrying Curt’s Special Recipe<sup>TM</sup></h2>
                <h3 class="hdg hdg_2 mix-txt_dark">Search: Minnesota | Wisconsin</h3>
                <div class="storeList">
                <ul class="hList storeList">
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                </ul>

                </div>
            </div>
        </section>

        <section class="wrapperFull wrapperFull_spread wrapperFull_light txtCenter">
            <h4 class="hdg hdg_1 mix-txt_block_lrg mix-txt_dark">Come out to one of these great locations or purchase your fix of Curt’s Special Recipe<sup>TM</sup> from our online store.</h4>
            <a class="btn btn_std btn_dark center" href="shop.php">GO BUY SOME!</a>
        </section>

        <?php include("includes/footer.php"); ?>

    </body>
</html>
