/**
 * cbpScroller.js v1.0.0
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2013, Codrops
 * http://www.codrops.com
 */(function(e){"use strict";function n(){var n=t.clientHeight,r=e.innerHeight;return n<r?r:n}function r(){return e.pageYOffset||t.scrollTop}function i(e){var t=0,n=0;do{isNaN(e.offsetTop)||(t+=e.offsetTop);isNaN(e.offsetLeft)||(n+=e.offsetLeft)}while(e=e.offsetParent);return{top:t,left:n}}function s(e,t){var s=e.offsetHeight,o=r(),u=o+n(),a=i(e).top,f=a+s,t=t||0;return a+s*t<=u&&f>=o}function o(e,t){for(var n in t)t.hasOwnProperty(n)&&(e[n]=t[n]);return e}function u(e,t){this.el=e;this.options=o(this.defaults,t);this._init()}var t=e.document.documentElement;u.prototype={defaults:{viewportFactor:.2},_init:function(){if(Modernizr.touch)return;this.sections=Array.prototype.slice.call(this.el.querySelectorAll(".slideInGroup"));this.didScroll=!1;var t=this;this.sections.forEach(function(e,t){s(e)||classie.add(e,"cbp-so-init")});var n=function(){if(!t.didScroll){t.didScroll=!0;setTimeout(function(){t._scrollPage()},60)}},r=function(){function e(){t._scrollPage();t.resizeTimeout=null}t.resizeTimeout&&clearTimeout(t.resizeTimeout);t.resizeTimeout=setTimeout(e,200)};e.addEventListener("scroll",n,!1);e.addEventListener("resize",r,!1)},_scrollPage:function(){var e=this;this.sections.forEach(function(t,n){if(s(t,e.options.viewportFactor))classie.add(t,"cbp-so-animate");else{classie.add(t,"cbp-so-init");classie.remove(t,"cbp-so-animate")}});this.didScroll=!1}};e.cbpScroller=u})(window);