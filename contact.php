<?php include("includes/head.php"); ?>
<?php include("includes/header.php"); ?>

        <section class="wrapperFull wrapperFull_wood wrapperFull_spread">
            <div class="wrapper">
                <div class="equalHeightCols">
                    <div class="equalHeightCols-col equalHeightCols-col_img">
                        <img class="img" src="assets/images/gallery/3-products.jpg" alt="" />
                    </div>
                    <div class="equalHeightCols-col equalHeightCols-col_dark">
                        <address class="contactCurts">
                            <h2 class="hdg hdg_1">Say hello.</h2>
                            <div class="bdcpy">Email: <a href="mailto:scott@curtssalsa.com" target="_top" class="text-link">scott@curtssalsa.com</a></div>
                            <div class="bdcpy">Phone: <a class="tel text-link">651.351.2797</a></div>
                            <div class="bdcpy">Fax: <a class=" tel text-link">651.351.0037</a></div>
                            
                            <br>
                            <div class="vcard bdcpy">
                                <p class="bdcpy">Mailing Address:
                                    <br>
                                    <span class="adr">
                                        <a href="https://maps.google.com/maps?q=402+North+Main+Street+Stillwater,+Minnesota+55082&hl=en&ll=45.058752,-92.807769&spn=0.004608,0.010986&sll=45.003198,-92.979196&sspn=0.295187,0.703125&t=h&hnear=402+Main+St+N,+Stillwater,+Minnesota+55082&z=17" target="_blank" class="text-link">
                                            <span>Curt’s Salsa</span><br>
                                            <span class="street-address">402 North Main Street</span><br>
                                            <span class="locality">Stillwater</span>,
                                            <span class="region">MN</span>
                                            <span class="postal-code">55082</span>
                                        </a>
                                    </span>
                                </p>
                            </div>

                            <p class="bdcpy">Follow Us:</p><br>
                            <a><i class="icon icon_accent icon-inline icon-facebook-circled"></i></a>
                            <a><i class="icon icon_accent icon-inline icon-twitter-circled"></i></a>
        
                            <a href="wheretobuy.php" class="textLink textLink_em">Find a location near you</a>
                        </address> 
                    </div>
                </div>
            </div>
        </section>

        <?php include("includes/footer.php"); ?>

    </body>
</html>
