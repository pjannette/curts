    </main>
    <!-- footer -->
    <footer class="globalFooter">
    	<div class="globalFooter-icons">
	    	<a><i class="icon icon-inline icon-facebook-circled"></i></a>
	    	<a><i class="icon icon-inline icon-twitter-circled globalFooter-icons_last"></i></a>
    	</div>
    	<div class="globalFooter-legalCopy">
    		<small>&copy; Curt's Special Recipe<sup>TM</sup> / Hand-Crafted Small Batch Sauces / Made in MN / All Rights Reserved.</small>
    	</div>
    </footer>
    
	<script>
        new cbpScroller( document.getElementById( 'js-slideInOnScroll' ));
	</script>

    <!-- Snipcart -->
        <script type="text/javascript" id="snipcart" src="https://app.snipcart.com/scripts/snipcart.js" data-api-key="MWQ4YWFmNmEtZDg2My00NzY3LTkzNTYtYzU0ZjUxNDkwNDY1"></script>
    <!-- end footer -->

