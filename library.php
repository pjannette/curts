<?php include("includes/head.php"); ?>
<?php include("includes/header.php"); ?>

        <div class="wrapperFull wrapperFull_wood">
            <div class="wrapper">
                <div class="flexsliderPlaceHolder">
                    <h2>
                        <span class="hdg hdg_1">Welcome to the</span>
                        <span class="hdg hdg_huge">Pattern Library</span>
                        <span class="hdg hdg_lrg">for Curt's.</span>
                    </h2>
                    <p class="bdcpy bdcpy_lrg mix-txt_light">For nearly 20 years we've been making salsa and now BBQ sauce and Bloody Mary Mix, the way it’s meant to be made: in small batches, using the best ingredients we can find, and of course, Curt's original signature blend of herbs and spices.</p>
                    <a class="btn btn_std btn_light">Standard Button CTA!</a>
                </div>
            </div>
        </div>

        <section class="wrapperFull wrapperFull_spread wrapperFull_white">
            <div class="wrapper grid">
                <h2 class="hdg hdg_1 testHeading mix-txt_dark">Typography</h2><br>
                <div class="grid-col grid-col_6">
                    <p class="bdcpy">Heading Styles - Helvetica Bold</p>
                    <p class="hdg hdg_1">Heading Style 1</p>
                    <p class="hdg hdg_1 mix-txt_dark">Heading Style 1 - Mix Style Dark</p>
                    <p class="hdg hdg_1 mix-txt_accent">Heading Style 1 - Mix Style Accent</p>
                    <p class="hdg hdg_2">Heading Style 2</p>
                    <p class="hdg hdg_2 mix-txt_dark">Heading Style 2 - Mix Style Dark</p>
                    
                </div>
                <div class="grid-col grid-col_6">
                    <p class="bdcpy">Body Copy - Helvetica Regular</p>

                    <p class="bdcpy bdcpy_lrg">Large Copy Large - Strip steak shankle tenderloin brisket kevin ball tip turkey doner rump flank cow ground round chicken biltong. Short ribs cow meatloaf corned beef venison capicola.</p>

                    <p class="bdcpy">Standard Body Copy - Ground round brisket tongue rump chicken. Leberkas meatloaf hamburger sausage sirloin boudin. Strip steak shankle tenderloin brisket kevin ball tip turkey doner rump.</p>

                    <p class="bdcpy bdcpy_sm">Small Body Copy - Tenderloin brisket kevin ball tip turkey doner rump flank cow ground round chicken biltong. Short ribs cow meatloaf corned beef venison capicola t-bone andouille ham hock strip steak doner hamburger.</p>

                    <p class="bdcpy mix-txt_light">Standard Body Copy - Mix Style Light</p>
                    <p class="bdcpy mix-txt_dark">Standard Body Copy - Mix Style Dark</p>
                </div>
                <br><br>
                <div class="grid-col grid-col_6">
                <br><br>
                    <p class="hdg hdg_1 mix-txt_dark">Heading 1 - Dark Mix</p>
                    <p class="hdg hdg_2 mix-txt_accent">Heading 2 - Dark Mix</p>
                    <p class="bdcpy">Bacon ipsum dolor sit amet tongue kevin short loin, brisket boudin bresaola capicola leberkas venison ham ribeye pancetta. Shankle ribeye hamburger ham. Flank kielbasa sausage drumstick capicola pig swine. Tail ribeye tri-tip, salami meatloaf kielbasa pastrami flank ham leberkas beef ribs sausage short ribs pork belly. Pork pork loin spare ribs pastrami biltong. Meatloaf bacon pastrami, shoulder beef ribs venison tenderloin filet mignon chicken sausage kevin tongue. Tenderloin tri-tip ham hock strip steak shank chicken cow spare ribs venison fatback meatloaf.</p>
                </div>
                <div class="grid-col grid-col_6">
                <br><br>
                    <p class="hdg hdg_2 mix-txt_accent">Heading Style 2</p>
                    <p class="bdcpy bdcpy_sm">Bacon ipsum dolor sit amet tongue kevin short loin, brisket boudin bresaola capicola leberkas venison ham ribeye pancetta.</p>
                    <p class="bdcpy bdcpy_sm">Shankle ribeye hamburger ham. Flank kielbasa sausage drumstick capicola pig swine. Tail ribeye tri-tip, salami meatloaf kielbasa pastrami flank ham leberkas beef ribs sausage short ribs pork belly.</p>

                    <p class="hdg hdg_2 mix-txt_accent">Heading Style 2</p>
                    <p class="bdcpy bdcpy_sm">Bacon ipsum dolor sit amet tongue kevin short loin, brisket boudin bresaola capicola leberkas venison ham ribeye pancetta.</p>
                    <p class="bdcpy bdcpy_sm">Shankle ribeye hamburger ham. Flank kielbasa sausage drumstick capicola pig swine. Tail ribeye tri-tip, salami meatloaf kielbasa pastrami flank ham leberkas beef ribs sausage short ribs pork belly.</p>
                </div>
            </div>
        </section>

        <section class="wrapperFull wrapperFull_spread wrapperFull_wood">
            <div class="wrapper grid">
                <h2 class="hdg hdg_1 testHeading">Links and Buttons</h2><br>
                <div class="grid-col grid-col_12 hasTestStyles hasTestStylesButtons">
                    <a class="btn">Button</a>
                    <a class="btn btn_std">Button Standard</a>
                    <a class="btn btn_std btn_dark">Dark Button</a>
                    <a class="btn btn_std btn_light">Light Button</a>
                    <a href="index.php" class="hdg hdg_1 textLink">Text Link - Heading Mix</a>
                    <a href="index.php" class="hdg hdg_1 textLink textLink_em">Text Link - With Underline</a>
                    <a href="index.php" class="bdcpy textLink">Text Link - Body Copy Mix</a> 
                </div>
            </div>
        </section>

        <!-- SHOP SECTION -->
        <section class="wrapperFull wrapperFull_light wrapperFull_spread">
            <div class="wrapper">
                <h2 class="hdg hdg_1 testHeading mix-txt_dark">Product Blocks</h2>
                <ul class="productList">
                    <li id="" class="collection">
                        <h3 class="hdg hdg_1 mix-txt_dark">Salsa</h3>
                        <hr> 
                        <ul class="productList-collection">
                            <li>
                                <article class="shopProduct">
                                    <div class="shopProduct-title hdg hdg_1">Mild Salsa</div>
                                    <figure class="shopProduct-image">
                                        <img src="http://placehold.it/230x210" alt="FPO" />
                                        <figcaption class="shopProduct-description bdcpy">Looking for salsa with training wheels? Here's one for the beginners out there. Tomatoes, onions, peppers and spices give you the salsa flavor you're looking for with just a hint of heat to tickle your taste buds.</figcaption>
                                    </figure>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">13.49 / 3 pack</div>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">25.49 / 6 pack</div>
                                    <a href="" class="shopProduct-addToCart btn btn_dark">Add to Cart</a>
                                </article>
                            </li>
                            <li>
                                <article class="shopProduct">
                                    <div class="shopProduct-title hdg hdg_1">Mild Salsa</div>
                                    <figure class="shopProduct-image">
                                        <img src="http://placehold.it/230x210" alt="FPO" />
                                        <figcaption class="shopProduct-description bdcpy">Looking for salsa with training wheels? Here's one for the beginners out there. Tomatoes, onions, peppers and spices give you the salsa flavor you're looking for with just a hint of heat to tickle your taste buds.</figcaption>
                                    </figure>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">13.49 / 3 pack</div>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">25.49 / 6 pack</div>
                                    <a href="" class="shopProduct-addToCart btn btn_dark">Add to Cart</a>
                                </article>
                            </li>
                            <li>
                                <article class="shopProduct">
                                    <div class="shopProduct-title hdg hdg_1">Mild Salsa</div>
                                    <figure class="shopProduct-image">
                                        <img src="http://placehold.it/230x210" alt="FPO" />
                                        <figcaption class="shopProduct-description bdcpy">Looking for salsa with training wheels? Here's one for the beginners out there. Tomatoes, onions, peppers and spices give you the salsa flavor you're looking for with just a hint of heat to tickle your taste buds.</figcaption>
                                    </figure>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">13.49 / 3 pack</div>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">25.49 / 6 pack</div>
                                    <a href="" class="shopProduct-addToCart btn btn_dark">Add to Cart</a>
                                </article>
                            </li>
                            <li>
                                <article class="shopProduct">
                                    <div class="shopProduct-title hdg hdg_1">Mild Salsa</div>
                                    <figure class="shopProduct-image">
                                        <img src="http://placehold.it/230x210" alt="FPO" />
                                        <figcaption class="shopProduct-description bdcpy">Looking for salsa with training wheels? Here's one for the beginners out there. Tomatoes, onions, peppers and spices give you the salsa flavor you're looking for with just a hint of heat to tickle your taste buds.</figcaption>
                                    </figure>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">13.49 / 3 pack</div>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">25.49 / 6 pack</div>
                                    <a href="" class="shopProduct-addToCart btn btn_dark">Add to Cart</a>
                                </article>
                            </li>
                            <li>
                                <article class="shopProduct">
                                    <div class="shopProduct-title hdg hdg_1">Mild Salsa</div>
                                    <figure class="shopProduct-image">
                                        <img src="http://placehold.it/230x210" alt="FPO" />
                                        <figcaption class="shopProduct-description bdcpy">Looking for salsa with training wheels? Here's one for the beginners out there. Tomatoes, onions, peppers and spices give you the salsa flavor you're looking for with just a hint of heat to tickle your taste buds.</figcaption>
                                    </figure>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">13.49 / 3 pack</div>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">25.49 / 6 pack</div>
                                    <a href="" class="shopProduct-addToCart btn btn_dark">Add to Cart</a>
                                </article>
                            </li>
                            <li>
                                <article class="shopProduct">
                                    <div class="shopProduct-title hdg hdg_1">Mild Salsa</div>
                                    <figure class="shopProduct-image">
                                        <img src="http://placehold.it/230x210" alt="FPO" />
                                        <figcaption class="shopProduct-description bdcpy">Looking for salsa with training wheels? Here's one for the beginners out there. Tomatoes, onions, peppers and spices give you the salsa flavor you're looking for with just a hint of heat to tickle your taste buds.</figcaption>
                                    </figure>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">13.49 / 3 pack</div>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">25.49 / 6 pack</div>
                                    <a href="" class="shopProduct-addToCart btn btn_dark">Add to Cart</a>
                                </article>
                            </li>
                            <li>
                                <article class="shopProduct">
                                    <div class="shopProduct-title hdg hdg_1">Mild Salsa</div>
                                    <figure class="shopProduct-image">
                                        <img src="http://placehold.it/230x210" alt="FPO" />
                                        <figcaption class="shopProduct-description bdcpy">Looking for salsa with training wheels? Here's one for the beginners out there. Tomatoes, onions, peppers and spices give you the salsa flavor you're looking for with just a hint of heat to tickle your taste buds.</figcaption>
                                    </figure>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">13.49 / 3 pack</div>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">25.49 / 6 pack</div>
                                    <a href="" class="shopProduct-addToCart btn btn_dark">Add to Cart</a>
                                </article>
                            </li>
                        </ul>    
                    </li>
                    <li class="collection">
                        <h3 class="hdg hdg_1 mix-txt_dark">Barbeque Sauce</h3>
                        <hr> 
                        <ul class="productList-collection">
                            <li>
                                <article class="shopProduct">
                                    <div class="shopProduct-title hdg hdg_1">Mild Salsa</div>
                                    <figure class="shopProduct-image">
                                        <img src="http://placehold.it/230x210" alt="FPO" />
                                        <figcaption class="shopProduct-description bdcpy">Looking for salsa with training wheels? Here's one for the beginners out there. Tomatoes, onions, peppers and spices give you the salsa flavor you're looking for with just a hint of heat to tickle your taste buds.</figcaption>
                                    </figure>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">13.49 / 3 pack</div>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">25.49 / 6 pack</div>
                                    <a href="" class="shopProduct-addToCart btn btn_dark">Add to Cart</a>
                                </article>
                            </li>
                            <li>
                                <article class="shopProduct">
                                    <div class="shopProduct-title hdg hdg_1">Mild Salsa</div>
                                    <figure class="shopProduct-image">
                                        <img src="http://placehold.it/230x210" alt="FPO" />
                                        <figcaption class="shopProduct-description bdcpy">Looking for salsa with training wheels? Here's one for the beginners out there. Tomatoes, onions, peppers and spices give you the salsa flavor you're looking for with just a hint of heat to tickle your taste buds.</figcaption>
                                    </figure>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">13.49 / 3 pack</div>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">25.49 / 6 pack</div>
                                    <a href="" class="shopProduct-addToCart btn btn_dark">Add to Cart</a>
                                </article>
                            </li>
                            <li>
                                <article class="shopProduct">
                                    <div class="shopProduct-title hdg hdg_1">Mild Salsa</div>
                                    <figure class="shopProduct-image">
                                        <img src="http://placehold.it/230x210" alt="FPO" />
                                        <figcaption class="shopProduct-description bdcpy">Looking for salsa with training wheels? Here's one for the beginners out there. Tomatoes, onions, peppers and spices give you the salsa flavor you're looking for with just a hint of heat to tickle your taste buds.</figcaption>
                                    </figure>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">13.49 / 3 pack</div>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">25.49 / 6 pack</div>
                                    <a href="" class="shopProduct-addToCart btn btn_dark">Add to Cart</a>
                                </article>
                            </li>
                        </ul>    
                    </li>
                </ul> 
            </div>
        </section>

        <section class="wrapperFull wrapperFull_wood wrapperFull_spread">
            <div class="wrapper hasTestStyles">
                <h2 class="hdg hdg_1 testHeading">Icons</h2><br>
                <a><i class="icon icon_light icon-inline icon-curts"></i></a>
                <a><i class="icon icon_light icon-inline icon-jar"></i></a>
                <a><i class="icon icon_light icon-inline icon-facebook-circled"></i></a>
                <a><i class="icon icon_light icon-inline icon-twitter-circled"></i></a>
                <a><i class="icon icon_light icon-inline icon-cart"></i></a>
                <a><i class="icon icon_light icon-inline icon-burger"></i></a>
                <a><i class="icon icon_light icon-inline icon-cub"></i></a>
                <a><i class="icon icon_light icon-inline icon-target"></i></a>
                <a><i class="icon icon_light icon-inline icon-lunds"></i></a>
                <a><i class="icon icon_light icon-inline icon-byerlys"></i></a>
                <a><i class="icon icon_light icon-inline icon-kowalskis"></i></a>
                <a><i class="icon icon_light icon-inline icon-hornbachers"></i></a>
                <a><i class="icon icon_light icon-inline icon-festival"></i></a>
            </div>
        </section>

        <section class="wrapperFull wrapperFull_dark wrapperFull_spread">
            <div class="wrapper">
                <h2 class="hdg hdg_1 testHeading">Publicity Links</h2><br>
                    <ul class="hList publicity">
                        <li class="btn btn_sm btn_light"><a>Small Press Link</a></li>
                        <li class="btn btn_lrg btn_light"><a>Small Press Link</a></li>
                        <li class="btn btn_sm btn_light"><a>Small Press Link</a></li>
                        <li class="btn btn_lrg btn_light"><a>Small Press Link</a></li>
                        <li class="btn btn_sm btn_light"><a>Small Press Link</a></li>
                        <li class="btn btn_sm btn_light"><a>Small Press Link</a></li>
                        <li class="btn btn_sm btn_light"><a>Small Press Link</a></li>
                        <li class="btn btn_lrg btn_light"><a>Small Press Link</a></li>
                        <li class="btn btn_sm btn_light"><a>Small Press Link</a></li>
                    </ul>

                </div>
           
        </section>

        <!-- MEDIA OBJECT -->
        <section class="wrapperFull wrapperFull_light">
            <div class="wrapper">
            <div class="hdg hdg_1 testHeading test_spread">Media Object</div>
                <div class="media">
                    <article class="media-bd grid-col grid-col_7">
                        <h2 class="hdg hdg_1 mix-txt_dark">Media Object - Media Body Heading</h2>
                        <p class="bdcpy">Media Body Body Copy - Out in the Wisconsin woods, Curt and Betty Hollister, two self-described foodies, perfected their savory blend of tomatoes, onions and peppers and began selling their homemade salsa at local farmer’s markets, under the eponymous name Curt's “Special Recipe” Salsa.</p>
                    </article>
                    <figure class="media-media grid-col grid-col_4">
                            <img class="img " src="http://placehold.it/330x330" alt="Empty Salsa Jars" />                                 
                    </figure>
                </div>
            </div>
        </section>

        <section class="wrapperFull wrapperFull_dark">
            <div class="media media_flip wrapper">
                <article class="media-bd grid-col grid-col_7">
                    <h2 class="hdg hdg_1">Media Object - Flipped  - Media Body Heading</h2>
                    <p class="bdcpy">Media Body Body Copy - Out in the Wisconsin woods, Curt and Betty Hollister, two self-described foodies, perfected their savory blend of tomatoes, onions and peppers and began selling their homemade salsa at local farmer’s markets, under the eponymous name Curt's “Special Recipe” Salsa.</p>
                </article>
                <figure class="media-media grid-col grid-col_4">
                        <img class="img " src="http://placehold.it/330x330" alt="Empty Salsa Jars" />                                 
                </figure>
            </div>
        </section>


        <!-- Slide In Objects -->
        <section id="js-slideInOnScroll" class="slideInOnScrollWrapper">
            <section class="wrapperFull wrapperFull_light">
                <div class="wrapper">
                    <div class="media slideInGroup">
                        <article class="media-bd grid-col grid-col_7">
                            <div class="slideInObject slideInObject_left">
                                <h2 class="hdg hdg_1 mix-txt_dark">Media Object w/ Slide In Effect - Media Body Heading</h2>
                                <p class="bdcpy">Out in the Wisconsin woods, Curt and Betty Hollister, two self-described foodies, perfected their savory blend of tomatoes, onions and peppers and began selling their homemade salsa at local farmer’s markets, under the eponymous name Curt's “Special Recipe” Salsa.</p>
                            </div>
                        </article>
                        
                        <figure class="media-media grid-col grid-col_4">
                            <div class="slideInObject slideInObject_right">
                                <img class="img " src="http://placehold.it/330x330" alt="Empty Salsa Jars" />                                 
                            </div>
                        </figure>
                    </div>
                </div>
            </section>

            <section class="wrapperFull wrapperFull_dark">
                <div class="media media_flip slideInGroup wrapper">
                    <article class="media-bd grid-col grid-col_7">
                        <div class="slideInObject slideInObject_right">
                            <h2 class="hdg hdg_1">Media Object w/ Slide In Effect - Flipped - Media Body Heading</h2>
                            <p class="bdcpy">Out in the Wisconsin woods, Curt and Betty Hollister, two self-described foodies, perfected their savory blend of tomatoes, onions and peppers and began selling their homemade salsa at local farmer’s markets, under the eponymous name Curt's “Special Recipe” Salsa.</p>
                        </div>
                    </article>
                    
                    <figure class="media-media grid-col grid-col_4">
                        <div class="slideInObject slideInObject_left">
                            <img class="img " src="http://placehold.it/330x330" alt="Empty Salsa Jars" />                                 
                        </div>
                    </figure>
                </div>
            </section>
        </section>

        <section class="wrapperFull wrapperFull_white wrapperFull_spread hasTestStyles"><!-- Grid System -->
            <div class="wrapper">
                <div class="section-hd">
                    <div class="hdg hdg_1 testHeading">Grid System<span class="bdcpy mix-txt_accent">12 Column</span></div>
                </div>
                <div class="grid">
                    <div class="grid-col grid-col_2">2</div>
                    <div class="grid-col grid-col_2 grid-col_push2">2 - push2</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_3 grid-col_push2">3 - push2</div>
                </div>
                <div class="grid">
                    <div class="grid-col grid-col_6 grid-col_push5">6 - push5</div>
                    <div class="grid-col grid-col_1">1</div>
                </div>
                <div class="grid">
                    <div class="grid-col grid-col_4">4</div>
                    <div class="grid-col grid-col_4 grid-col_push4">4 - push4</div>
                </div>
                <div class="grid">
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                    <div class="grid-col grid-col_1">1</div>
                </div>
                <div class="grid">
                    <div class="grid-col grid-col_2">2</div>
                    <div class="grid-col grid-col_2">2</div>
                    <div class="grid-col grid-col_2">2</div>
                    <div class="grid-col grid-col_2">2</div>
                    <div class="grid-col grid-col_2">2</div>
                    <div class="grid-col grid-col_2">2</div>
                </div>
                <div class="grid">
                    <div class="grid-col grid-col_3">3</div>
                    <div class="grid-col grid-col_3">3</div>
                    <div class="grid-col grid-col_3">3</div>
                    <div class="grid-col grid-col_3">3</div>
                </div>
                <div class="grid">
                    <div class="grid-col grid-col_4">4</div>
                    <div class="grid-col grid-col_4">4</div>
                    <div class="grid-col grid-col_4">4</div>
                </div>
                <div class="grid">
                    <div class="grid-col grid-col_5">5</div>
                    <div class="grid-col grid-col_2">2</div>
                    <div class="grid-col grid-col_5">5</div>
                </div>
                <div class="grid">
                    <div class="grid-col grid-col_6">6</div>
                    <div class="grid-col grid-col_6">6</div>
                </div>
                <div class="grid">
                    <div class="grid-col grid-col_5">5</div>
                    <div class="grid-col grid-col_7">7</div>
                </div>
                <div class="grid">
                    <div class="grid-col grid-col_4">4</div>
                    <div class="grid-col grid-col_8">8</div>
                </div>
            </div>  
        </section>

        <section class="imageCarousel">
            <!-- FLEXSLIDER Small -->
            <div class="flexslider flexslider_sm js-flexslider_sm carousel">
                <!-- Images demensions should be around 455x300 -->
                <ul class="slides">
                    <li><img src="http://placehold.it/455x300" alt=" "></li>
                    <li><img src="http://placehold.it/455x300" alt=" "></li>
                    <li><img src="assets/images/slider-sm/jarandpeppers.jpg" alt=" "></li>
                    <li><img src="http://placehold.it/455x300" alt=" "></li>
                    <li><img src="assets/images/slider-sm/ingredients.jpg" alt=" "></li>
                    <li><img src="http://placehold.it/455x300" alt=" "></li>
                    <li><img src="assets/images/slider-sm/bloody-mary-mix.jpg" alt=" "></li>
                    <li><img src="http://placehold.it/455x300" alt=" "></li>                           
                </ul>
            </div>
        </section>

        <section class="wrapperFull wrapperFull_spread wrapperFull_wood">
            <div class="gallery">
                <h2 class="hdg hdg_1 center center_all testHeading">Image Gallery</h2>
                <h2 class="hdg hdg_1 center center_all">It's what's behind the label that really stands out.</h2>
                <figure class="imageGallery">
                    <ul>
                        <li><img src="assets/images/gallery/3-products.jpg" alt="" class="galleryImg_tall" /></li>
                        <li><img src="assets/images/gallery/fresh-tomatoes-medium.jpg" alt="" class="galleryImg_med" /></li>
                        <li><img src="assets/images/gallery/curts-bbq.jpg" alt="" class="galleryImg_med" /></li>
                        <li class="imageGallery-groupOf4">
                            <ul>
                                <li><img src="assets/images/gallery/beans-and-spices.jpg" alt="" class="galleryImg_sm" /></li>
                                <li><img src="assets/images/gallery/salsa-and-ingredients.jpg" alt="" class="galleryImg_sm" /></li>
                                <li><img src="assets/images/gallery/bloody-mary-mix.jpg" alt="" class="galleryImg_sm" /></li>
                                <li><img src="assets/images/gallery/chips-and-salsa.jpg" alt="" class="galleryImg_sm" /></li>
                            </ul>
                        </li>             
                        <li><img src="assets/images/gallery/bloody-mary-mix-goodness.jpg" alt="" class="galleryImg_tall_skinny" /></li>
                        <li class="imageGallery-groupOf2"> 
                            <ul>
                                <li><img src="assets/images/gallery/fresh-onions.jpg" alt="" class="galleryImg_med" /></li>
                                <li><img src="assets/images/gallery/black-bean-salsa.jpg" alt="" class="galleryImg_med" /></li> 
                            </ul>
                        </li>
                        <li><img src="assets/images/gallery/fresh-tomato-large.jpg" alt="" class="galleryImg_tall" /></li>
                    </ul>
                    <figcaption class="galleryDescription">An image gallery of Curt's Special Recipe's fresh ingredients and hand-crafted salsa, barbeque sauce, and bloody mary mix.</figcaption>
                </figure>
            </div>
        </section>

        <section class="wrapperFull wrapperFull_spread wrapperFull_wood">
            <div class="wrapper">
            <div class="hdg hdg_1 testHeading">Two Column Equal Height</div>
                <div class="equalHeightCols">
                    <div class="equalHeightCols-col equalHeightCols-col_dark">
                        Column with Content<br>
                        Column with Content<br>
                        Column with Content<br>
                        Column with Content<br>
                        Column with Content<br>
                        Column with Content<br>
                        Column with Content<br>
                        Column with Content<br>
                        Column with Content<br>
                        Column with Content<br>
                        Column with Content<br>
                    </div>
                    <div class="equalHeightCols-col equalHeightCols-col_light">
                        Column with Content<br>
                        Column with Content<br>
                        Column with Content<br>
                        Column with Content<br>
                        Column with Content<br>
                        Column with Content<br>
                        Column with Content<br>              
                    </div>
                </div>
            </div>
        </section>

        <section class="wrapperFull wrapperFull_spread wrapperFull_wood">
            <div class="wrapper">
            <div class="hdg hdg_1 testHeading">Two Column Equal Height</div>
                <div class="equalHeightCols">
                    <div class="equalHeightCols-col equalHeightCols-col_img">  
                        <img class="img" src="http://www.fillmurray.com/500/640" alt=" " />
                    </div>
                    <div class="equalHeightCols-col equalHeightCols-col_dark">
                        Column Number Two Content
                        Column Number Two Content<br>
                        Column Number Two Content<br>
                        Column Number Two Content<br>
                        Column Number Two Content<br>
                        Column Number Two Content<br>
                        Column Number Two Content<br>
                        Column Number Two Content<br>
                        Column Number Two Content<br>
                        Column Number Two Content<br>
                        Column Number Two Content<br>
                        Column Number Two Content<br>
                        Column Number Two Content<br>
                        Column Number Two Content<br>
                        Column Number Two Content<br>
                        Column Number Two Content<br>
                        Column Number Two Content<br>
                    </div>
                </div>
            </div>
        </section>

        <section class="wrapperFull wrapperFull_white wrapperFull_spread">
            <div class="wrapper">
                <div class="hdg hdg_1 testHeading mix-txt_dark">Store Location Column Layout</div>

                <h2 class="hdg hdg_1 mix-txt_dark">More fine establishments carrying Curt’s Special Recipe<sup>TM</sup></h2>
                <h3 class="hdg hdg_2 mix-txt_dark">Search: Minnesota | Wisconsin</h3>
                <div class="storeList">
                <ul class="hList multiRowList">
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                    <li class="vcard">
                        <p class="bdcpy">
                            <span class="adr">
                                <span class="hdg hdg_2 mix-txt_accent">Store Title</span>
                                <span class="street-address">Street Address</span><br>
                                <span class="locality">City</span>,
                                <span class="region">State</span>
                                <span class="postal-code">Zip</span>
                            </span>
                        </p>
                    </li>
                </ul>

                </div>
            </div>
        </section>

         <section class="section section_light section-spread_large">
            <h4 class="hdg hdg_1 mix-txt_dark center center_all">Super Sweet Pre Footer Content!</h4>
            <a class="btn btn_std btn_dark center center_all">Standard Button CTA!</a>
        </section>
        
        <?php include("includes/footer.php"); ?>

    </body>
</html>
