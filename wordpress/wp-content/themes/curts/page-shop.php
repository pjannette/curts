<?php
/**
 * Template name: Shop
 * @package curts
 */

get_header(); ?>

		<header class="wrapperFull wrapperFull_wood wrapperFull_spread">
            <div class="wrapper">
                <h2 class="hdg hdg_1">Get ‘em while they’re hot. Or in some cases, mild.</h2>
                <p class="bdcpy mix-txt_light">All Products come in packs of 3 and 6.</p>
            </div>
        </header>

        <section class="wrapperFull wrapperFull_white wrapperFull_spread browserMessage">
        	<p class="bdcpy">Your web browser is out of date. Please upgrade it if you wish to purchase products online. We recommend getting <a class="textLink browserMessage-link" href="https://www.google.com/intl/en/chrome/browser/?platform=win" target="_blank">Google Chrome.</a> It's easy, fast, safe, and free. We appologize for the inconvenience.</p>
        </section>

        <section class="wrapperFull wrapperFull_light">
            <div class="wrapper">
                <div class="group">
                    <h2 class="isVisuallyHidden">Shop Curt's Special Recipe Salsa, barbecue, and Bloody Mary Mix</h2><br>
                    <div class="dropDownMenu dd dropDownMenu_products" tabindex="1">
                        <span class="hdg hdg_2 mix-txt_dark dropDownMenu-title"> Product Type: </span>
                        <ul class="dropDownMenu-list js-simpleSearchList simpleSearchList">
                            <li class="is-currentFilter" data="all">All</li>
                            <li data="salsa">Salsa</li>
                            <li data="barbecue">Barbecue Sauce</li>
                            <li data="bloody mary mix">Bloody Mary Mix</li>
                            <li data="apparel">Apparel</li>
                            <li data="quart">Quarts</li>
                        </ul>
                    </div>
                </div>

			   <ul class="productList js-searchableList">

			    <!-- Salsa -->
				<?php
	    		$salsa_query = new WP_Query(
				  array(
				  'post_type' => 'products',
				  'category_name' => 'salsa'
				) ); ?>

				<li class="searchable collection" data-index="salsa quart all">
                    <h3 class="hdg hdg_1 mix-txt_dark">Salsa</h3>
                    <hr> 
                    <ul class="productList-collection">
						<?php while ( $salsa_query -> have_posts()) : $salsa_query -> the_post(); ?>
						<?php get_template_part('mods/shop-Product');?>
                		<?php endwhile;  ?>
                	</ul>
				 </li>

				 <!-- Barbecue Sauce -->
				 <?php wp_reset_postdata();
				 $bbq_query = new WP_Query(
				  array(
				  'post_type' => 'products',
				  'category_name' => 'barbecue'
				) ); ?>

				<li class="searchable collection" data-index="barbecue pint all">
                    <h3 class="hdg hdg_1 mix-txt_dark">Barbecue</h3>
                    <hr> 
                    <ul class="productList-collection">
						<?php while ( $bbq_query -> have_posts()) : $bbq_query -> the_post(); ?>
							<?php get_template_part('mods/shop-Product');?>
                		<?php endwhile;  ?>
                	</ul>
				 </li>

				 <!-- Bloody Mary Mix -->
				 <?php wp_reset_postdata();
				 $bmm_query = new WP_Query(
				  array(
				  'post_type' => 'products',
				  'category_name' => 'bloody mary mix'
				) ); ?>

				<li class="searchable collection" data-index="bloody mary mix pint all">
                    <h3 class="hdg hdg_1 mix-txt_dark">Bloody Mary Mix</h3>
                    <hr> 
                    <ul class="productList-collection">
						<?php while ( $bmm_query -> have_posts()) : $bmm_query -> the_post(); ?>
							<?php get_template_part('mods/shop-Product');?>
                		<?php endwhile;  ?>
                	</ul>
				 </li>

				 <!-- Apparel -->
				 <?php wp_reset_postdata();
				 $apparel_query = new WP_Query(
				  array(
				  'post_type' => 'products',
				  'category_name' => 'apparel'
				) ); ?>

				<li class="searchable collection" data-index="apparel all">
                    <h3 class="hdg hdg_1 mix-txt_dark">Apparel</h3>
                    <hr> 
                    <ul class="productList-collection">
						<?php while ( $apparel_query -> have_posts()) : $apparel_query -> the_post(); ?>
							<?php get_template_part('mods/shop-Product');?>
                		<?php endwhile;  ?>
                	</ul>
				 </li>

				</ul> 
            </div>
        </section>

        <section class="wrapperFull wrapperFull_spread wrapperFull_white txtCenter">
            <h4 class="hdg hdg_1 mix-txt_dark">Or find us at one of these great locations!</h4>
            <a class="btn btn_std btn_dark center" href="<?php echo esc_url( home_url( '/' ) ); ?>locations">FIND A STORE</a>
        </section>

<?php get_footer(); ?>
