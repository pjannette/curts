<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package curts
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <!-- ICONS -->
    <link rel="shortcut icon" type="image/ico" href="http://192.168.168.184:8888/wordpress/wordpress/wp-content/uploads/2013/10/favicon.png" />
    <link rel="apple-touch-icon" href="http://192.168.168.184:8888/wordpress/wordpress/wp-content/uploads/2013/10/apple-touch-icon.png" />
    <!-- TYPEKIT -->
    <script type="text/javascript" src="//use.typekit.net/odh1wpv.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
    
    <?php wp_head(); ?>
</head>

<body <?php body_class('cbp-spmenu-push'); ?>>

	<!-- header -->
    <header id="siteHeader" class="globalHeader lTable" role="banner">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="globalLogo" title="Go to the homepage">
            <i class="icon icon_light icon-curts"></i>
            <h1 class="globalLogo-text">Curt's Special Recipe, Small Batch Sasla, Barbeque Sauce, and Bloody Mary Mix</h1>
        </a>    

        <nav class="globalNav cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="cbp-spmenu-s2" role="navigation">
        	<div class="skip-link"><a class="screen-reader-text" href="#content"><?php _e( 'Skip to content', 'curts' ); ?></a></div>
        	<div class="isHidden_desktop globalNav-title"><i class="icon icon_light icon-curts"></i></div>
        	<ul class="globalNav-list">
				<li<?php wp_reset_query(); if ( is_front_page() ) { echo ' class="globalNav-list_current"'; } ?>><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><span data-hover="Home">Home</span></a></li>
                <li<?php if ( is_page('gallery')) { echo ' class="globalNav-list_current"'; } ?>><a href="<?php echo esc_url( home_url( '/' ) ); ?>gallery"><span data-hover="Flavors">Gallery</span></a></li>
                <li<?php if ( is_page('locations')) { echo ' class="globalNav-list_current"'; } ?>><a href="<?php echo esc_url( home_url( '/' ) ); ?>locations"><span data-hover="Where to Buy Curt's">Where to Buy</span></a></li>
                <li<?php if ( is_page('contact')) { echo ' class="globalNav-list_current"'; } ?>><a href="<?php echo esc_url( home_url( '/' ) ); ?>contact"><span data-hover="Contact">Contact</span></a></li>
                <li<?php if ( is_page('shop')) { echo ' class="globalNav-list_current"'; } ?>><a href="<?php echo esc_url( home_url( '/' ) ); ?>shop"><span data-hover="Shop">Shop</span></a></li>
                <li class="globalHeader-cart">
                    <a href="#" class="snipcart-checkout snipcart-summary js-closeMobileMenu">
                        <span class="icon icon-basket globalHeader-cart_icon"></span>
                        <span class="globalHeader-cart_text">(<span class="snipcart-total-items"></span>)</span>
                    </a>
                </li>
			</ul>
        </nav>
        <div id="js-showRightPush" class="mobileMenuIcon icon"></div>
    </header>
    <!-- end header -->

    <div class="toTop js-scrollTop">
        <i class="icon icon-jar"></i>
    </div>
    
    <main class="main" role="main">
