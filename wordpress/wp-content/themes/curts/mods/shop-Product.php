                        
                        <li class="searchable" data-index="<?php the_field("product_filter_tags"); ?> all">
                            <article class="shopProduct">
                                <div class="shopProduct-title hdg hdg_2"><?php the_title(); ?></div>
                                <figure class="shopProduct-image">
                                    <?php the_post_thumbnail(); ?>
                                    <figcaption class="shopProduct-description bdcpy"><?php the_content(); ?></figcaption>
                                </figure>
                                <div class="bdcpy bdcpy_sm mix-txt_accent"><?php the_field("pricing_info"); ?></div>
                                <div class="bdcpy bdcpy_sm mix-txt_accent"><?php the_field("alt_pricing_info"); ?></div>
                                <a class="snipcart-add-item shopProduct-addToCart btn btn_dark"
                                href="#" data-item-id="<?php the_field("sc_product_id"); ?>" 
                                data-item-name="<?php the_field("sc_product_name"); ?>" 
                                data-item-price="<?php the_field("sc_product_price"); ?>" 
                                data-item-weight="<?php the_field("sc_product_weight"); ?>" 
                                data-item-url="http://www.curtsspecialrecipe.com/shop.php" 
                                data-item-description="<?php the_field("sc_item_description"); ?>">Add to Cart</a>
                            </article>
                        </li>