<?php
/**
 * Template name: Home
 * @package curts
 */

get_header(); ?>
        <div class="wrapperFull">
            <div class="introSection js-introSection">
                <figure class="introSection-background">
                    <img class="introSection-background_img" src="<?php echo get_template_directory_uri(); ?>/static/assets/images/maters.jpg" alt=" " />
                    <img class="introSection-background_img" src="<?php echo get_template_directory_uri(); ?>/static/assets/images/curts-special-recipe.jpg" alt="Empty Salsa Jars" />
                    <img class="introSection-background_img" src="<?php echo get_template_directory_uri(); ?>/static/assets/images/curts-fresh-ingredients.jpg" alt=" " />   
                </figure>
                <div class="wrapper wrapper_cover introSection-textBlock">
                    <h2>
                        <span class="hdg mix-txt_intro">Ordinary Label.</span>
                        <span class="hdg hdg_huge">Extraordinary</span>
                        <span class="hdg hdg_lrg">Ingredients.</span>
                    </h2>
                    <p class="bdcpy bdcpy_lrg mix-txt_light"><?php the_field("paragraph"); ?></p>
                    <a class="btn btn_std btn_light" href="<?php echo esc_url( home_url( '/' ) ); ?>shop">Go Buy Some!</a>
                </div>
            </div>
        </div>
        <section id="js-slideInOnScroll" class="slideInOnScrollWrapper">
            <section class="wrapperFull wrapperFull_light">
                <div class="wrapper">
                	<!-- Salsa -->
					<?php
		    		$beginnings_query = new WP_Query(
					  array(
					  'post_type' => 'text_blocks',
					  'category_name' => 'beginnings'
					) ); ?>
					<?php while ( $beginnings_query -> have_posts()) : $beginnings_query -> the_post(); ?>
                    <div class="media slideInGroup">
                        <article class="media-bd grid-col grid-col_7 grid-col_pushHalf">
                            <div class="slideInObject slideInObject_left">
                                <h3 class="hdg hdg_1 mix-txt_dark"><?php the_title(); ?></h3>
                                <p class="bdcpy"><?php the_field("body_copy"); ?></p>
                            </div>
                        </article>
                        
                        <figure class="media-media grid-col grid-col_4">
                            <div class="slideInObject slideInObject_right">
                                <?php the_post_thumbnail('thumbnail', array('class' => 'img img_rounded img_offset')); ?>                                 
                            </div>
                        </figure>
                    </div>
                    <?php endwhile;  ?>
                </div>
            </section>

            <section class="wrapperFull wrapperFull_dark">
            	<!-- Salsa -->
				<?php
                wp_reset_postdata();
	    		$craft_query = new WP_Query(
				  array(
				  'post_type' => 'text_blocks',
				  'category_name' => 'craft'
				) ); ?>
				<?php while ( $craft_query -> have_posts()) : $craft_query -> the_post(); ?>
				<div class="media media_flip slideInGroup wrapper">
					<article class="media-bd grid-col grid-col_7">
						<div class="slideInObject slideInObject_right">
							<h3 class="hdg hdg_1"><?php the_title(); ?></h3>
							<p class="bdcpy"><?php the_field("body_copy"); ?></p>
                        </div>
                    </article>
                    <figure class="media-media grid-col grid-col_4">
                        <div class="slideInObject slideInObject_left">
                            <?php the_post_thumbnail('thumbnail', array('class' => 'img img_rounded img_offset')); ?>
                        </div>
                    </figure>
                </div>
                <?php endwhile;  ?>
            </section>

            <section class="wrapperFull wrapperFull_light">
                <div class="wrapper">
                	<!-- Salsa -->
					<?php
                    wp_reset_postdata();
		    		$label_query = new WP_Query(
					  array(
					  'post_type' => 'text_blocks',
					  'category_name' => 'its whats inside'
					) ); ?>
					<?php while ( $label_query -> have_posts()) : $label_query -> the_post(); ?>
                    <div class="media slideInGroup">
                        <article class="media-bd grid-col grid-col_7 grid-col_pushHalf">
                            <div class="slideInObject slideInObject_left">
                                <h3 class="hdg hdg_1 mix-txt_dark"><?php the_title(); ?></h3>
                                <p class="bdcpy"><?php the_field("body_copy"); ?></p>
                            </div>
                        </article>
                        
                        <figure class="media-media grid-col grid-col_4">
                            <div class="slideInObject slideInObject_right">
                                <?php the_post_thumbnail('thumbnail', array('class' => 'img img_rounded img_offset')); ?>                                 
                            </div>
                        </figure>
                    </div>
                    <?php endwhile;  ?>
                    <?php wp_reset_postdata(); ?>
                </div>
            </section>
        </section>

        <section class="slider">
            <div class="flexslider flexslider_sm js-flexslider_sm carousel">
                <!-- Images demensions should be around 455x300 -->
                <ul class="slides">
                    <li><img src="<?php echo get_template_directory_uri(); ?>/static/assets/images/slider-sm/jarandpeppers.jpg" alt=" "></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/static/assets/images/slider-sm/peppers.jpg" alt=" "></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/static/assets/images/gallery/fresh-salsa-ingredients.jpg" alt=" "></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/static/assets/images/slider-sm/tomatoes-clams.jpg" alt=" "></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/static/assets/images/slider-sm/peppers-and-tomatoes.jpg" alt=" "></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/static/assets/images/slider-sm/bloody-mary-mix.jpg" alt=" "></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/static/assets/images/slider-sm/ingredients.jpg" alt=" "></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/static/assets/images/slider-sm/spicy-barbeque.jpg" alt=" "></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/static/assets/images/gallery/fresh-tomatoes.jpg" alt=" "></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/static/assets/images/slider-sm/vegetables.jpg" alt=" "></li>
                    <li><img src="<?php echo get_template_directory_uri(); ?>/static/assets/images/gallery/spices.jpg" alt=" "></li>             
                    <li><img src="<?php echo get_template_directory_uri(); ?>/static/assets/images/gallery/fresh-salsa-ingredients-2.jpg" alt=" "></li>
                    
                </ul>
            </div>
        </section>

        <section class="wrapperFull wrapperFull_wood wrapperFull_spread">
            <div class="wrapper txtCenter">
                <h3 class="hdg hdg_1">Some of the nice things people have to say about us:</h3>
                <ul class="publicity">
                <?php while(has_sub_field("publicity_link_row")): ?>
                    <?php if(get_row_layout() == "1_2_1"): ?>
                        <?php  if(get_sub_field("short_link_1_name") != ''): ?>

                        <li>
                            <a class="btn btn_sm btn_light" href="<?php the_sub_field("short_link_1_url"); ?>" target="_blank" title="View the article on the <?php the_sub_field("short_link_1_name"); ?> website"><?php the_sub_field("short_link_1_name"); ?></a>
                        </li>
                        <?php endif;  ?>
                        <?php  if(get_sub_field("long_link_name") != ''): ?>

                        <li>
                            <a class="btn btn_lrg btn_light" href="<?php the_sub_field("long_link_url"); ?>" target="_blank" title="View the article on the <?php the_sub_field("long_link_name"); ?> website"><?php the_sub_field("long_link_name"); ?></a>
                        </li>
                        <?php endif;  ?>
                        <?php  if(get_sub_field("short_link_2_name") != ''): ?>

                        <li>
                            <a class="btn btn_sm btn_light" href="<?php the_sub_field("short_link_2_url"); ?>" target="_blank" title="View the article on the <?php the_sub_field("short_link_2_name"); ?> website"><?php the_sub_field("short_link_2_name"); ?></a>
                        </li>
                       <?php endif; ?>
                        <?php elseif(get_row_layout() == "2_1_1"): ?>
                        <?php  if(get_sub_field("long_link_name") != ''): ?>

                        <li>
                            <a class="btn btn_lrg btn_light" href="<?php the_sub_field("long_link_url"); ?>" target="_blank" title="View the article on the <?php the_sub_field("long_link_name"); ?> website"><?php the_sub_field("long_link_name"); ?></a>
                        </li>
                        <?php endif;  ?>
                        <?php  if(get_sub_field("short_link_1_name") != ''): ?>

                        <li>
                            <a class="btn btn_sm btn_light" href="<?php the_sub_field("short_link_1_url"); ?>" target="_blank" title="View the article on the <?php the_sub_field("short_link_1_name"); ?> website"><?php the_sub_field("short_link_1_name"); ?></a>
                        </li>
                        <?php endif;  ?>
                        <?php  if(get_sub_field("short_link_2_name") != ''): ?>

                        <li>
                            <a class="btn btn_sm btn_light" href="<?php the_sub_field("short_link_2_url"); ?>" target="_blank" title="View the article on the <?php the_sub_field("short_link_2_name"); ?> website"><?php the_sub_field("short_link_2_name"); ?></a>
                        </li>
                        <?php endif; ?>
                        <?php elseif(get_row_layout() == "1_1_2"): ?>
                        <?php  if(get_sub_field("short_link_1_name") != ''): ?>

                        <li>
                            <a class="btn btn_sm btn_light" href="<?php the_sub_field("short_link_1_url"); ?>" target="_blank" title="View the article on the <?php the_sub_field("short_link_1_name"); ?> website"><?php the_sub_field("short_link_1_name"); ?></a>
                        </li>
                        <?php endif;  ?>
                        <?php  if(get_sub_field("short_link_2_name") != ''): ?>

                        <li>
                            <a class="btn btn_sm btn_light" href="<?php the_sub_field("short_link_2_url"); ?>" target="_blank" title="View the article on the <?php the_sub_field("short_link_2_name"); ?> website"><?php the_sub_field("short_link_2_name"); ?></a>
                        </li>
                       <?php endif; ?>
                       <?php  if(get_sub_field("long_link_name") != ''): ?>

                        <li>
                            <a class="btn btn_lrg btn_light" href="<?php the_sub_field("long_link_url"); ?>" target="_blank" title="View the article on the <?php the_sub_field("long_link_name"); ?> website"><?php the_sub_field("long_link_name"); ?></a>
                        </li>
                        <?php endif;  ?>

                    <?php endif; ?>
                <?php endwhile; ?>
                </ul>
                <p class="hdg hdg_2">Would You Like to Write About Curt’s? <a href="mailto:scott@curtssalsa.com" class="textLink mix-txt_underline">Email Us.</a></p>
            </div>
        </section>

        <section class="wrapperFull wrapperFull_spread wrapperFull_light txtCenter">
            <h4 class="hdg hdg_1 mix-txt_dark">What are you waiting for? Go grab a jar or two!</h4>
            <a class="btn btn_std btn_dark center" href="<?php echo esc_url( home_url( '/' ) ); ?>shop">GO BUY SOME!</a>
        </section>

<?php get_footer(); ?>
