<?php
/**
 * curts functions and definitions
 *
 * @package curts
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 640; /* pixels */

if ( ! function_exists( 'curts_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function curts_setup() {

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on curts, use a find and replace
	 * to change 'curts' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'curts', get_template_directory() . '/languages' );

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for Post Thumbnails on posts and pages
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'curts' ),
	) );

	/**
	 * Enable support for Post Formats
	 */
	add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );

	/**
	 * Setup the WordPress core custom background feature.
	 */
	add_theme_support( 'custom-background', apply_filters( 'curts_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // curts_setup
add_action( 'after_setup_theme', 'curts_setup' );

/**
 * Register widgetized area and update sidebar with default widgets
 */
function curts_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'curts' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'curts_widgets_init' );

/**
 * Enqueue scripts and styles
 */

function curts_scripts() {
	wp_enqueue_style( 'curts-style', get_template_directory_uri() . '/static/assets/css/screen.css' );

	if(preg_match('/(?i)msie [2-8]/',$_SERVER['HTTP_USER_AGENT']))
	{
	    wp_enqueue_script( 'shiv-js', get_template_directory_uri() . '/static/assets/scripts/html5shiv.js');
	    wp_enqueue_script( 'respond-js', get_template_directory_uri() . '/static/assets/scripts/respond.min.js');
	    wp_enqueue_style( 'curts-ie8-style', get_template_directory_uri() . '/static/assets/css/ie8.css' );
	}

	if(preg_match('/(?i)msie [1-9]/',$_SERVER['HTTP_USER_AGENT']))
	{
	    wp_enqueue_style( 'curts-ie-style', get_template_directory_uri() . '/static/assets/css/ie.css' );
	}
	wp_enqueue_script( 'curts-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );
	
	wp_enqueue_script( 'classie-js', get_template_directory_uri() . '/static/assets/scripts/classie.js', array('jquery'), '', true );

	if ( is_front_page() ) {
		wp_enqueue_script( 'flexslider-js', get_template_directory_uri() . '/static/assets/scripts/jquery.flexslider-min.js', array('jquery'));
		wp_enqueue_script( 'modernizr-js', get_template_directory_uri() . '/static/assets/scripts/modernizr.custom.js', array('jquery'));
		wp_enqueue_script( 'cbpScroller-js', get_template_directory_uri() . '/static/assets/scripts/cbpScroller.js', array('jquery'));
	}

	// if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
	// 	wp_enqueue_script( 'comment-reply' );
	// }

	wp_enqueue_script( 'curts-js', get_template_directory_uri() . '/static/assets/scripts/global.js', array('jquery'));

}
add_action( 'wp_enqueue_scripts', 'curts_scripts' );

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
