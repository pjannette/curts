<?php
/**
 * Template name: Gallery
 * @package curts
 */

get_header(); ?>

		<?php while ( have_posts() ) : the_post(); ?>

			<?php // get_template_part( 'content', 'page' ); ?>
		<section class="wrapperFull wrapperFull_wood wrapperFull_spread"><!-- Grid System -->
            <div class="wrapper">
            <h2 class="hdg hdg_1 center center_all">It's what's behind the label that really stands out.</h2>

                <figure class="imageGallery">             
                    <div class="span12">
                        <div class=" span6"><img src="<?php echo get_template_directory_uri(); ?>/static/assets/images/gallery/3-products.jpg" alt="" /></div>
                        <div class="span6">
                            <div class="span12"><img src="<?php echo get_template_directory_uri(); ?>/static/assets/images/gallery/fresh-tomatoes.jpg" alt="" /></div>
                            <div class="span12"><img class="imgAdjustMobile" src="<?php echo get_template_directory_uri(); ?>/static/assets/images/gallery/curts-bbq.jpg" alt="" /></div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="span8">
                            <div class="span6"><img src="<?php echo get_template_directory_uri(); ?>/static/assets/images/gallery/beans-and-spices.jpg" alt="" /></div>
                            <div class="span6"><img src="<?php echo get_template_directory_uri(); ?>/static/assets/images/gallery/salsa-and-ingredients.jpg" alt="" /></div>
                            <div class="span6"><img class="imgAdjustMobile" src="<?php echo get_template_directory_uri(); ?>/static/assets/images/gallery/bloody-mary-mix.jpg" alt="" /></div>
                            <div class="span6"><img class="imgAdjustMobile" src="<?php echo get_template_directory_uri(); ?>/static/assets/images/gallery/chips-and-salsa.jpg" alt="" /></div>
                        </div>

                        <div class="span4"><img src="<?php echo get_template_directory_uri(); ?>/static/assets/images/gallery/bloody-mary-mix-goodness.jpg" alt="" /></div>
                    </div>

                    <div class="span12">
                        <div class="span12"><img src="<?php echo get_template_directory_uri(); ?>/static/assets/images/gallery/spices2.jpg" alt=" "></div>
                    </div>
                    
                    <div class="span6">
                        <div class="span12"><img src="<?php echo get_template_directory_uri(); ?>/static/assets/images/gallery/fresh-onions.jpg" alt="" /></div>
                        <div class="span12"><img class="imgAdjustMobile" src="<?php echo get_template_directory_uri(); ?>/static/assets/images/gallery/black-bean-salsa.jpg" alt="" /></div>
                    </div>
                    <div class="span6"><img src="<?php echo get_template_directory_uri(); ?>/static/assets/images/gallery/fresh-tomato-large.jpg" alt="" /></div>

                    <div class="span12">
                        <div class="span8"><img src="<?php echo get_template_directory_uri(); ?>/static/assets/images/gallery/fresh-salsa-ingredients.jpg" alt=" "></div>
                        <div class="span4"><img src="<?php echo get_template_directory_uri(); ?>/static/assets/images/gallery/chips.jpg" alt="" /></div>
                    </div>
                    <div class="span12">
                        <div class="span6"><img src="<?php echo get_template_directory_uri(); ?>/static/assets/images/gallery/fresh-salsa-and-ingredients.jpg" alt="" /></div>
                        <div class="span6"><img src="<?php echo get_template_directory_uri(); ?>/static/assets/images/gallery/spices.jpg" alt="" /></div>
                        <div class="span6"><img src="<?php echo get_template_directory_uri(); ?>/static/assets/images/slider-sm/spicy-barbeque.jpg" alt="" /></div>
                        <div class="span6"><img src="<?php echo get_template_directory_uri(); ?>/static/assets/images/slider-sm/peppers-and-tomatoes.jpg" alt="" /></div>
                    </div>
                    <figcaption class="imageGallery-Description">An image gallery of Curt's Special Recipe's fresh ingredients and hand-crafted salsa, barbeque sauce, and bloody mary mix.</figcaption>
                </figure>  
            </div>
        </section>
    
        <section class="wrapperFull wrapperFull_spread wrapperFull_light txtCenter">
            <h4 class="hdg hdg_1 mix-txt_dark">What are you waiting for? Go grab a jar or two!</h4>
            <a class="btn btn_std btn_dark center" href="<?php echo esc_url( home_url( '/' ) ); ?>shop">GO BUY SOME!</a>
        </section>

		<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>
