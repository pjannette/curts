<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package curts
 */

get_header(); ?>

		<section class="wrapperFull wrapperFull_wood wrapperFull_spread">
            <div class="wrapper">
                <h2 class="hdg hdg_huge mix-txt_404">We're Sorry, this page does not exist.<br> Please use the navigation menu to get back to a working page on our website.</h2>
            </div>
        </section>

<?php get_footer(); ?>