<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package curts
 */
?>

	</main>
    <!-- footer -->
    <footer class="globalFooter">
    	<div class="globalFooter-icons">
	    	<a href="https://www.facebook.com/pages/Curts-Salsa/79296246092" target="_blank" title="Go to our facebook page"><i class="icon icon-inline icon-facebook-circled"></i></a>
            <a href="http://instagram.com/curtssalsa" target="_blank" title="View Our Instagram feed"><i class="icon icon-inline icon-instagram"></i></a>
	    	<a href="https://twitter.com/curtssalsa" target="_blank" title="View our Twitter feed"><i class="icon icon-inline icon-twitter-circled globalFooter-icons_last"></i></a>
    	</div>
    	<div class="globalFooter-legalCopy">
    		<small>&copy; Curt's Special Recipe<sup>TM</sup>  /  Hand-Crafted Small Batch Sauces  /  Made in MN  /  All Rights Reserved.</small>
    	</div>
    </footer>
    
    <script>
       // new cbpScroller( document.getElementById( 'js-slideInOnScroll' ));
	</script>
    <!-- Snipcart -->
    <script type="text/javascript" id="snipcart" src="https://app.snipcart.com/scripts/snipcart.js" data-api-key="YzdlYjZhOWMtNDQ2MS00NzhiLTlkNWYtNGRiMWM2OTVkNWRm"></script>
    <!-- end footer -->

<?php wp_footer(); ?>

</body>
</html>