<?php
/**
 * Template name: Contact
 * @package curts
 */

get_header(); ?>

		<section class="wrapperFull wrapperFull_wood wrapperFull_spread">
            <div class="wrapper">
                <div class="equalHeightCols">
                    <div class="equalHeightCols-col equalHeightCols-col_img">
                        <img class="img" src="<?php echo get_template_directory_uri(); ?>/static/assets/images/gallery/fresh-salsa-ingredients-2.jpg" alt="" />
                    </div>
                    <div class="equalHeightCols-col equalHeightCols-col_dark">
                        <address class="contactCurts">
                            <h2 class="hdg hdg_1">Say hello.</h2>
                            <div class="bdcpy">Email: <a href="mailto:<?php the_field("email"); ?>" target="_top" class="text-link"><?php the_field("email"); ?></a></div>
                            <div class="bdcpy">Phone: <a class="tel text-link"><?php the_field("phone"); ?></a></div>
                            <div class="bdcpy">Fax: <a class=" tel text-link"><?php the_field("fax"); ?></a></div>
                            
                            <br>
                            
                            <?php while(has_sub_field("mailing_address")): ?>
                            <?php if(get_row_layout() == "address"): // layout: Content ?>
                            <div class="vcard bdcpy">
                                <p class="bdcpy">Mailing Address:
                                    <br>
                                    <span class="adr">
                                        <a href="https://maps.google.com/maps?q=402+North+Main+Street+Stillwater,+Minnesota+55082&hl=en&ll=45.058752,-92.807769&spn=0.004608,0.010986&sll=45.003198,-92.979196&sspn=0.295187,0.703125&t=h&hnear=402+Main+St+N,+Stillwater,+Minnesota+55082&z=17" target="_blank" class="text-link">
                                            <span><?php the_sub_field("address_title"); ?></span><br>
                                            <span class="street-address"><?php the_sub_field("address_line_1"); ?></span><br>
                                            <span class="locality"><?php the_sub_field("city_name"); ?></span>,
                                            <span class="region"><?php the_sub_field("state"); ?></span>
                                            <span class="postal-code"><?php the_sub_field("zip_code"); ?></span>
                                        </a>
                                    </span>
                                </p>
                            </div>
                            <?php endif; ?>
                            <?php endwhile; ?> 

                            <p class="bdcpy">Follow Us:</p><br>
                            <a href="https://www.facebook.com/pages/Curts-Salsa/79296246092" target="_blank" title="Go to our facebook page"><i class="icon icon-inline icon-facebook-circled"></i></a>
                            <a href="https://twitter.com/curtssalsa" target="_blank" title="View our Twitter feed"><i class="icon icon-inline icon-twitter-circled globalFooter-icons_last"></i></a>
        
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>locations" class="textLink textLink_em">Find a location near you</a>
                        </address> 
                    </div>
                </div>
            </div>
        </section>

<?php get_footer(); ?>
