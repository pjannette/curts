<?php
/**
 * Template name: Locations
 * @package curts
 */

get_header(); ?>
		<section class="wrapperFull wrapperFull_wood wrapperFull_spread">
            <div class="wrapper">

                
                <div class="ourLocations">
                <h2 class="hdg hdg_1">Find us in these fine establishments</h2>
                    <div class="ourLocations-retailLinks">
                        <a href="http://www.cub.com/" target="_blank"><i class="icon icon_light icon_buyLink icon-cub"></i></a>
                        <a href="http://celebrate.festivalfoods.net/locations/" target="_blank"><i class="icon icon_light icon_buyLink icon-festival"></i></a>
                        <a href="http://www.target.com/store-locator/find-stores#?lnk=fnav_t_spc_1_1" target="_blank"><i class="icon icon_light icon_buyLink icon-target"></i></a>
                        <a href="http://www.lundsandbyerlys.com/" target="_blank"><i class="icon icon_light icon_buyLink icon-lunds"></i></a>
                        <a href="http://www.lundsandbyerlys.com/" target="_blank"><i class="icon icon_light icon_buyLink icon-byerlys"></i></a>
                        <a href="http://www.kowalskis.com/" target="_blank"><i class="icon icon_light icon_buyLink icon_buyLink_sm icon-kowalskis"></i></a>
                        <a href="http://www.hornbachers.com/" target="_blank"><i class="icon icon_light icon_buyLink icon_buyLink_sm2 icon-hornbachers"></i></a>
                        <a href="http://www.rainbowfoods.com/" target="_blank"><i class="icon icon_light icon_buyLink icon_buyLink_sm icon-rainbow"></i></a>
                        <a href="hhttp://www.copps.com/" target="_blank"><i class="icon icon_light icon_buyLink icon_buyLink_sm icon-copps"></i></a>
                        <a href="http://www.picknsave.com/" target="_blank"><i class="icon icon_light icon_buyLink icon_buyLink_sm icon-picknsave"></i></a>
                        <a href="http://www.metromarket.net/" target="_blank"><i class="icon icon_light icon_buyLink icon_buyLink_sm icon-metro-market"></i></a>
                        <a href="http://www.marianos.com/" target="_blank"><i class="icon icon_light icon_buyLink icon_buyLink_sm icon-marianos_fresh_market"></i></a>
                    </div>
                </div>             

            </div>
        </section>

        <section class="wrapperFull wrapperFull_white wrapperFull_spread">
            <div class="wrapper">
                <h2 class="hdg hdg_1 mix-txt_dark">More fine establishments carrying Curt’s Special Recipe<sup>TM</sup></h2>
                
                <form class="js-simpleSearchForm simpleSearchForm">
                    <label class="hdg hdg_2 mix-txt_dark">Find Curt's Special Recipe Near:</label>
                    <input id="js-simpleSearchInput" class="input input_nobrdr input_simpleSearch" type="search" placeholder="Enter the name of a city, state or zip code"/>
                    <input class="isVisuallyHidden js-submitIsDisabled" type="submit" />
                </form>

                <div class="storeLocations">
                    <ul class="storeLocations-list js-searchableList">
                        <?php
                        $store_locations_query = new WP_Query(
                          array(
                          'post_type' => 'store_location',
                          'order' => 'ASC',
                          'orderby' => 'title'
                        ) ); ?>
                        <?php while ( $store_locations_query -> have_posts()) : $store_locations_query -> the_post(); ?>
                        
                        <?php
                        $sString = array( get_the_title(), get_field('address_line_1'), get_field('city'), get_field('state'), get_field('zip')  );
                        $sArgs = implode(" ", $sString);
                        $lower = strtolower($sArgs);
                        ?>

                        <li class="vcard searchable" data-index="<?php echo $lower; ?>">
                            <p class="bdcpy">
                                <span class="adr">
                                    <span class="hdg hdg_2 mix-txt_accent"><?php the_title(); ?></span>
                                    <span class="street-address"><?php the_field("address_line_1"); ?></span><br>
                                    <?php  if(get_sub_field("address_line_2") != ''): ?>
                                    <span class="street-address-2"><?php the_field("address_line_2"); ?></span><br>
                                    <?php endif; ?>
                                    <span class="locality"><?php the_field("city"); ?></span>,
                                    <span class="region"><?php the_field("state"); ?></span>
                                    <span class="postal-code"><?php the_field("zip"); ?></span><br>
                                    <a class="textLink" href="<?php the_field("google_maps_link"); ?>" target="_blank">Get Directions</a>
                                </span>
                            </p>
                        </li>
                        <?php endwhile;  ?>
                    </ul>
                </div>
            </div>
        </section>

        <section class="wrapperFull wrapperFull_spread wrapperFull_light txtCenter">
            <h4 class="hdg hdg_1 mix-txt_block_lrg mix-txt_dark">Come out to one of these great locations or purchase your fix of Curt’s Special Recipe<sup>TM</sup> from our online store.</h4>
            <a class="btn btn_std btn_dark center" href="<?php echo esc_url( home_url( '/' ) ); ?>shop">GO BUY SOME!</a>
        </section>

		

        

<?php get_footer(); ?>
