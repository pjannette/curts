<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'curts_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'gNqU]W7vk,ePbAjF-)}~qv5Yv^S3yav8vS5Fp?nzfFi5#+^^5ifRYaSrrR/N}Mrx');
define('SECURE_AUTH_KEY',  '2)]-logXqPZZK?2qmaipEqVm=tO}a^ $mK>#CoCi=)8U8bQP> 3)d4(q>bqBK/Ly');
define('LOGGED_IN_KEY',    'h6=`Brkk{I;s==hOkBZHok:+hG/LhOYw?,V},K8t<[BU1m@Q6u--C3MiiC6]C)7|');
define('NONCE_KEY',        'ClHv&QY`WAS9ety-G/M9#p]D(u72.yetX>!#hO/2id!0/(a-j~+JqJgPmRB9?SqS');
define('AUTH_SALT',        '7r{x/6@DqTg. *y_*/5OGZ1|)m[-M`wEn,F-Zb>Z}ml/.ItRtE4bPsd%M:>Wpx-k');
define('SECURE_AUTH_SALT', 'WyPc0S-<=5P:mcaBZ!Y&t=1%*n^jOVosm{j#_[LBR=(=He$}(H-0VbFtw.$s#Ha~');
define('LOGGED_IN_SALT',   'j}Pjy.`y+.7#XNL%D|A7W4UfF7(YY0*T8@o9#%kBubwGRwr,IL(LIxqq3g}Bo-W8');
define('NONCE_SALT',       '2fZVeX0O*pAtkaP(|nR|v$*PIyP+TFyi-,c*Vz1A|)HY[#n*RV8~1DI7qHRUhd3-');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
