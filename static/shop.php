<?php include("includes/head.php"); ?>
<?php include("includes/header.php"); ?>

        <header class="wrapperFull wrapperFull_wood wrapperFull_spread">
            <div class="wrapper">
                <h2 class="hdg hdg_1">Get ‘em while they’re hot. Or in some cases, mild.</h2>
                <p class="bdcpy mix-txt_light">All Products come in packs of 3 and 6.</p>
            </div>
        </header>

        <section class="wrapperFull wrapperFull_light">
            <div class="wrapper">
                <div class="group">
                    <h2 class="isVisuallyHidden">Shop Curt's Special Recipe Salsa, Barbeque, and Bloody Mary Mix</h2><br>
                    <div id="" class="dropDownMenu dd dropDownMenu_products" tabindex="1">
                        <span class="hdg hdg_2 mix-txt_dark dropDownMenu-title"> Product Type: </span>
                        <ul class="dropDownMenu-list js-simpleSearchList simpleSearchList">
                            <li class="is-currentFilter" data="all">All</li>
                            <li data="salsa">Salsa</li>
                            <li data="barbeque">Barbeque Sauce</li>
                            <li data="bloody mary mix">Bloody Mary Mix</li>
                            <li data="apparel">Apparel</li>
                            <li data="quart">Quarts</li>
                        </ul>
                    </div>
            
                </div>

                <ul class="productList js-searchableList">
                    <li class="searchable collection" data-index="salsa pint all">
                        <h3 class="hdg hdg_1 mix-txt_dark">Salsa</h3>
                        <hr> 
                        <ul class="productList-collection">
                            <li>
                                <article class="shopProduct">
                                    <div class="shopProduct-title hdg hdg_2">Mild</div>
                                    <figure class="shopProduct-image">
                                        <img src="assets/images/products/Salsa_Mild.jpg" alt="FPO" />
                                        <figcaption class="shopProduct-description bdcpy">Looking for salsa with training wheels? Here's one for the beginners out there. Tomatoes, onions, peppers and spices give you the salsa flavor you're looking for with just a hint of heat to tickle your taste buds.</figcaption>
                                    </figure>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$13.49 / 3 pack</div>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$25.49 / 6 pack</div>
                                    <a class="snipcart-add-item shopProduct-addToCart btn btn_dark"
                                    href="#"
                                    data-item-id="2" 
                                    data-item-name="Mild Salsa" 
                                    data-item-price="13.49" 
                                    data-item-weight="20" 
                                    data-item-url="http://myapp.com/products/mild" 
                                    data-item-description="Mild Salsa">Add to Cart</a>
                                </article>
                            </li>
                            <li>
                                <article class="shopProduct">
                                    <div class="shopProduct-title hdg hdg_2">Medium</div>
                                    <figure class="shopProduct-image">
                                        <img src="assets/images/products/Salsa_Medium.jpg" alt="FPO" />
                                        <figcaption class="shopProduct-description bdcpy">Moved on from the mild? You're ready for Curt's Medium Salsa. Same ingredients as the mild, but we dump in enough jalapeño and banana peppers to give your mouth a bit of lingering heat after it's gone.</figcaption>
                                    </figure>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$13.49 / 3 pack</div>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$25.49 / 6 pack</div>
                                    <a class="snipcart-add-item shopProduct-addToCart btn btn_dark"
                                    href="#"
                                    data-item-id="3" 
                                    data-item-name="Medium Salsa" 
                                    data-item-price="13.49" 
                                    data-item-weight="20" 
                                    data-item-url="http://myapp.com/products/medium" 
                                    data-item-description="Medium Salsa">Add to Cart</a>
                                </article>
                            </li>
                            <li>
                                <article class="shopProduct">
                                    <div class="shopProduct-title hdg hdg_2">Hot</div>
                                    <figure class="shopProduct-image">
                                        <img src="assets/images/products/Salsa_Hot.jpg" alt="FPO" />
                                        <figcaption class="shopProduct-description bdcpy">Kicked up the heat another notch is exactly what we've done here. Same great flavor, more heat. "Hot, but not too hot" is what we typically hear. If your family likes a little heat, we think that you will love Curt's Hot Salsa.</figcaption>
                                    </figure>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$13.49 / 3 pack</div>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$25.49 / 6 pack</div>
                                    <a class="snipcart-add-item shopProduct-addToCart btn btn_dark"
                                    href="#"
                                    data-item-id="4" 
                                    data-item-name="Hot Salsa" 
                                    data-item-price="13.49" 
                                    data-item-weight="20" 
                                    data-item-url="http://myapp.com/products/hot" 
                                    data-item-description="Hot Salsa">Add to Cart</a>
                                </article>
                            </li>
                            <li>
                                <article class="shopProduct">
                                    <div class="shopProduct-title hdg hdg_2">Extra Hot Salsa</div>
                                    <figure class="shopProduct-image">
                                        <img src="assets/images/products/Salsa_ExtraHot.jpg" alt="FPO" />
                                        <figcaption class="shopProduct-description bdcpy">While the hot has nice heat we found ourselves looking for more. We took the hot salsa, added habanero peppers and viola! Heat without losing the flavor was the goal here, and we've done just that. We hope you like the added kick!</figcaption>
                                    </figure>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$13.49 / 3 pack</div>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$25.49 / 6 pack</div>
                                    <a class="snipcart-add-item shopProduct-addToCart btn btn_dark"
                                    href="#"
                                    data-item-id="5" 
                                    data-item-name="Extra Hot Salsa" 
                                    data-item-price="13.49" 
                                    data-item-weight="20" 
                                    data-item-url="http://myapp.com/products/extraHot" 
                                    data-item-description="Extra Hot Salsa">Add to Cart</a>
                                </article>
                            </li>
                            <li>
                                <article class="shopProduct">
                                    <div class="shopProduct-title hdg hdg_2">Triple Hot Salsa</div>
                                    <figure class="shopProduct-image">
                                        <img src="assets/images/products/Salsa_TripleHot.jpg" alt="FPO" />
                                        <figcaption class="shopProduct-description bdcpy">So you think you're a chili head? Well give our Triple Hot a try and we think you'll be pleased. We took our Extra Hot and tripled the amount of habaneros giving it the mouth watering burn you crave without sacrificing the flavor you have come to expect from Curt's Salsa. 2009 Scovie Award Winner for the habanero category.</figcaption>
                                    </figure>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$13.49 / 3 pack</div>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$25.49 / 6 pack</div>
                                    <a class="snipcart-add-item shopProduct-addToCart btn btn_dark"
                                    href="#"
                                    data-item-id="6" 
                                    data-item-name="Triple Hot Salsa" 
                                    data-item-price="13.49" 
                                    data-item-weight="20" 
                                    data-item-url="http://myapp.com/products/tripleHot" 
                                    data-item-description="Triple Hot Salsa">Add to Cart</a>
                                </article>
                            </li>
                            <li>
                                <article class="shopProduct">
                                    <div class="shopProduct-title hdg hdg_2">Black Bean Salsa</div>
                                    <figure class="shopProduct-image">
                                        <img src="assets/images/products/BlackBeanChipotle_MedHot.jpg" alt="FPO" />
                                        <figcaption class="shopProduct-description bdcpy">Feeling as though your salsa has bean missing something? A play off of our award winning original series, the black beans in this salsa add an earthy heartiness while the chipotle peppers balance with enough smooth smoky heat to be labeled “medium hot” by Curt’s standards.</figcaption>
                                    </figure>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$13.49 / 3 pack</div>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$25.49 / 6 pack</div>
                                    <a class="snipcart-add-item shopProduct-addToCart btn btn_dark"
                                    href="#"
                                    data-item-id="7" 
                                    data-item-name="Black Bean Salsa" 
                                    data-item-price="13.49" 
                                    data-item-weight="20" 
                                    data-item-url="http://myapp.com/products/blackBean" 
                                    data-item-description="Black Bean Salsa">Add to Cart</a>
                                </article>
                            </li>
                        </ul>        
                    </li>
                    <ul class="productList-collection searchable" data-index="salsa quart all">
                            <li>
                                <article class="shopProduct">
                                    <div class="shopProduct-title hdg hdg_2">Mild Salsa</div>
                                    <figure class="shopProduct-image">
                                        <img src="assets/images/products/Lg_Salsa_Mild.jpg" alt="FPO" />
                                        <figcaption class="shopProduct-description bdcpy">Looking for salsa with training wheels? Here's one for the beginners out there. Tomatoes, onions, peppers and spices give you the salsa flavor you're looking for with just a hint of heat to tickle your taste buds.</figcaption>
                                    </figure>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$19.99 / 3 pack</div>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$35.99 / 6 pack</div>
                                    <a class="snipcart-add-item shopProduct-addToCart btn btn_dark"
                                    href="#"
                                    data-item-id="2" 
                                    data-item-name="Mild Salsa" 
                                    data-item-price="13.49" 
                                    data-item-weight="20" 
                                    data-item-url="http://myapp.com/products/mild" 
                                    data-item-description="Mild Salsa">Add to Cart</a>
                                </article>
                            </li>
                            <li>
                                <article class="shopProduct">
                                    <div class="shopProduct-title hdg hdg_2">Medium Salsa</div>
                                    <figure class="shopProduct-image">
                                        <img src="assets/images/products/Lg_Salsa_Medium.jpg" alt="FPO" />
                                        <figcaption class="shopProduct-description bdcpy">Moved on from the mild? You're ready for Curt's Medium Salsa. Same ingredients as the mild, but we dump in enough jalapeño and banana peppers to give your mouth a bit of lingering heat after it's gone.</figcaption>
                                    </figure>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$19.99 / 3 pack</div>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$35.99 / 6 pack</div>
                                    <a class="snipcart-add-item shopProduct-addToCart btn btn_dark"
                                    href="#"
                                    data-item-id="3" 
                                    data-item-name="Medium Salsa" 
                                    data-item-price="13.49" 
                                    data-item-weight="20" 
                                    data-item-url="http://myapp.com/products/medium" 
                                    data-item-description="Medium Salsa">Add to Cart</a>
                                </article>
                            </li>
                            <li>
                                <article class="shopProduct">
                                    <div class="shopProduct-title hdg hdg_2">Hot Salsa</div>
                                    <figure class="shopProduct-image">
                                        <img src="assets/images/products/Lg_Salsa_Hot.jpg" alt="FPO" />
                                        <figcaption class="shopProduct-description bdcpy">Kicked up the heat another notch is exactly what we've done here. Same great flavor, more heat. "Hot, but not too hot" is what we typically hear. If your family likes a little heat, we think that you will love Curt's Hot Salsa.</figcaption>
                                    </figure>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$19.99 / 3 pack</div>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$35.99 / 6 pack</div>
                                    <a class="snipcart-add-item shopProduct-addToCart btn btn_dark"
                                    href="#"
                                    data-item-id="4" 
                                    data-item-name="Hot Salsa" 
                                    data-item-price="13.49" 
                                    data-item-weight="20" 
                                    data-item-url="http://myapp.com/products/hot" 
                                    data-item-description="Hot Salsa">Add to Cart</a>
                                </article>
                            </li>
                        </ul>
                    <li class="searchable collection" data-index="barbeque pint all">
                        <h3 class="hdg hdg_1 mix-txt_dark">Barbeque Sauce</h3>
                        <hr> 
                        <ul class="productList-collection">
                            <li>
                                <article class="shopProduct">
                                    <div class="shopProduct-title hdg hdg_2">Original BBQ</div>
                                    <figure class="shopProduct-image">
                                        <img src="assets/images/products/BBQ_Sauce_Original.jpg" alt="FPO" />
                                        <figcaption class="shopProduct-description bdcpy">Sweet, smoky and peppery are words often used to describe our original bbq sauce. Versatile is another. Brush generously onto ribs, chops, chicken, seafood – anything really – during the final moments on the grill. Or use it as a dipping sauce. Better yet, why not do both?</figcaption>
                                    </figure>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$13.49 / 3 pack</div>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$25.49 / 6 pack</div>
                                    <a class="snipcart-add-item shopProduct-addToCart btn btn_dark"
                                    href="#"
                                    data-item-id="8" 
                                    data-item-name="Original BBQ" 
                                    data-item-price="13.49" 
                                    data-item-weight="20" 
                                    data-item-url="http://myapp.com/products/OriginalBBQ" 
                                    data-item-description="Original BBQ">Add to Cart</a>
                                </article>
                            </li>
                            <li>
                                <article class="shopProduct">
                                    <div class="shopProduct-title hdg hdg_2">Spicy BBQ</div>
                                    <figure class="shopProduct-image">
                                        <img src="assets/images/products/BBQ_Sauce_Spicy.jpg" alt="FPO" />
                                        <figcaption class="shopProduct-description bdcpy">We’ve turned up the heat without drowning out the flavor. This stuff offers the same sweet, smoky, peppery flavor profile of our original BBQ sauce, but its been kicked up a Scoville or two for those who crave a little heat with their meat. Brush generously onto ribs, chops, chicken, seafood – anything really – during the final moments on the grill. Or use it as a dipping sauce. Better yet, why not do both?</figcaption>
                                    </figure>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$13.49 / 3 pack</div>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$25.49 / 6 pack</div>
                                    <a class="snipcart-add-item shopProduct-addToCart btn btn_dark"
                                    href="#"
                                    data-item-id="9" 
                                    data-item-name="Spicy BBQ" 
                                    data-item-price="13.49" 
                                    data-item-weight="20" 
                                    data-item-url="http://myapp.com/products/spicyBBQ" 
                                    data-item-description="Spicy BBQ">Add to Cart</a>
                                </article>
                            </li>
                        </ul>    
                    </li>
                    <li class="searchable collection" data-index="bloody mary mix pint all">
                        <h3 class="hdg hdg_1 mix-txt_dark">Bloody Mary Mix</h3>
                        <hr> 
                        <ul class="productList-collection">
                            <li>
                                <article class="shopProduct">
                                    <div class="shopProduct-title hdg hdg_2">Bloody Mary Mix</div>
                                    <figure class="shopProduct-image">
                                        <img src="assets/images/products/BloodyMaryMix_Original.jpg" alt="FPO" />
                                        <figcaption class="shopProduct-description bdcpy">We’ve been whipping up special recipe Bloody Marys for friends and family for years. And now we’re pleased to share this famous family recipe with you. Curt’s Bloody Mary Mix is fully seasoned and ready to use. Pour over ice with your favorite vodka, or simply enjoy on its own as a Virgin Mary. Garnish generously with vegetables, beef sticks, and cheeses. Repeat as necessary.</figcaption>
                                    </figure>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$13.49 / 3 pack</div>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$25.49 / 6 pack</div>
                                    <a class="snipcart-add-item shopProduct-addToCart btn btn_dark"
                                    href="#"
                                    data-item-id="10" 
                                    data-item-name="Bloody Mary Mix" 
                                    data-item-price="13.49" 
                                    data-item-weight="20" 
                                    data-item-url="http://myapp.com/products/bloodyMaryMix" 
                                    data-item-description="Bloody Mary Mix">Add to Cart</a>
                                </article>
                            </li>
                        </ul>    
                    </li>
                    <li class="searchable collection" data-index="apparel all">
                        <h3 class="hdg hdg_1 mix-txt_dark">Apparel</h3>
                        <hr> 
                        <ul class="productList-collection">
                            <li>
                                <article class="shopProduct">
                                    <div class="shopProduct-title hdg hdg_2">Men's T-Shirt</div>
                                    <figure class="shopProduct-image">
                                        <img src="assets/images/products/Mens_Cream_shirt.jpg" alt="FPO" />
                                        <figcaption class="shopProduct-description bdcpy">Cream Men's T-Shirt with Adam Turman artwork. </figcaption>
                                    </figure>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$13.49 / 3 pack</div>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$25.49 / 6 pack</div>
                                    <a class="snipcart-add-item shopProduct-addToCart btn btn_dark"
                                    href="#"
                                    data-item-id="11" 
                                    data-item-name="Men's Cream T-shirt" 
                                    data-item-price="15.00" 
                                    data-item-weight="20" 
                                    data-item-url="http://myapp.com/products/mens-cream" 
                                    data-item-description="Men's Cream T-shirt">Add to Cart</a>
                                </article>
                            </li>
                            <li>
                                <article class="shopProduct">
                                    <div class="shopProduct-title hdg hdg_2">Men's T-Shirt</div>
                                    <figure class="shopProduct-image">
                                        <img src="assets/images/products/Mens_Pebble_Brown_shirt.jpg" alt="FPO" />
                                        <figcaption class="shopProduct-description bdcpy">Pebble Brown Men's T-Shirt with Adam Turman artwork. </figcaption>
                                    </figure>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$13.49 / 3 pack</div>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$25.49 / 6 pack</div>
                                    <a class="snipcart-add-item shopProduct-addToCart btn btn_dark"
                                    href="#"
                                    data-item-id="12" 
                                    data-item-name="Men's Pebble Brown T-shirt" 
                                    data-item-price="15.00" 
                                    data-item-weight="20" 
                                    data-item-url="http://myapp.com/products/mens-pebble" 
                                    data-item-description="Men's Pebble Brown T-shirt">Add to Cart</a>
                                </article>
                            </li>
                            <li>
                                <article class="shopProduct">
                                    <div class="shopProduct-title hdg hdg_2">Women's T-Shirt</div>
                                    <figure class="shopProduct-image">
                                        <img src="assets/images/products/Womens_Cream_shirt.jpg" alt="FPO" />
                                        <figcaption class="shopProduct-description bdcpy">Cream Women's T-Shirt with Adam Turman artwork.</figcaption>
                                    </figure>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$13.49 / 3 pack</div>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$25.49 / 6 pack</div>
                                    <a class="snipcart-add-item shopProduct-addToCart btn btn_dark"
                                    href="#"
                                    data-item-id="13" 
                                    data-item-name="Women's Cream T-shirt" 
                                    data-item-price="15.00" 
                                    data-item-weight="20" 
                                    data-item-url="http://myapp.com/products/womens-cream" 
                                    data-item-description="Women's Cream T-shirt">Add to Cart</a>
                                </article>
                            </li>
                            <li>
                                <article class="shopProduct">
                                    <div class="shopProduct-title hdg hdg_2">Women's T-Shirt</div>
                                    <figure class="shopProduct-image">
                                        <img src="assets/images/products/Womens_Pebble_Brown_shirt.jpg" alt="FPO" />
                                        <figcaption class="shopProduct-description bdcpy">Pebble Brown Women's T-Shirt with Adam Turman artwork. </figcaption>
                                    </figure>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$13.49 / 3 pack</div>
                                    <div class="bdcpy bdcpy_sm mix-txt_accent">$25.49 / 6 pack</div>
                                    <a class="snipcart-add-item shopProduct-addToCart btn btn_dark"
                                    href="#"
                                    data-item-id="14" 
                                    data-item-name="Women's Cream T-shirt" 
                                    data-item-price="15.00" 
                                    data-item-weight="20" 
                                    data-item-url="http://myapp.com/products/womens-pebble" 
                                    data-item-description="Women's Pebble Brown T-shirt">Add to Cart</a>
                                </article>
                            </li>
                        </ul>    
                    </li>
                </ul>       
            </div>
        </section>

        <section class="wrapperFull wrapperFull_spread wrapperFull_white txtCenter">
            <h4 class="hdg hdg_1 mix-txt_dark">Or find us at one of these great locations!</h4>
            <a class="btn btn_std btn_dark center" href="wheretobuy.php">FIND A STORE</a>
        </section>

        <?php include("includes/footer.php"); ?>

    </body>
</html>
