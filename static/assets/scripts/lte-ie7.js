/* Load this script using conditional IE comments if you need to support IE 7 and IE 6. */

window.onload = function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'curts-icons\'">' + entity + '</span>' + html;
	}
	var icons = {
			'icon-cart' : '&#xe000;',
			'icon-cart-2' : '&#xe001;',
			'icon-cart-3' : '&#xe002;',
			'icon-phone-hang-up' : '&#xe003;',
			'icon-spinner' : '&#xe004;',
			'icon-fog' : '&#xe806;',
			'icon-left-1' : '&#xe887;',
			'icon-up' : '&#xe889;',
			'icon-right-thin' : '&#xe800;',
			'icon-right' : '&#xe888;',
			'icon-down' : '&#xe886;',
			'icon-down-dir' : '&#xe890;',
			'icon-th-list' : '&#xe849;',
			'icon-forward' : '&#xe8ab;',
			'icon-trash-1' : '&#xe87b;',
			'icon-basket-circled' : '&#xe876;',
			'icon-basket-1' : '&#xe875;',
			'icon-share' : '&#xe80c;',
			'icon-phone-circled' : '&#xe86f;',
			'icon-pencil-circled' : '&#xe8c7;',
			'icon-mail-circled' : '&#xe808;',
			'icon-location-2' : '&#xe894;',
			'icon-basket' : '&#xe80d;',
			'icon-fast-food' : '&#xe881;',
			'icon-spin6' : '&#xe837;',
			'icon-menu-2' : '&#xe856;',
			'icon-reply' : '&#xe8ac;',
			'icon-left' : '&#xe812;',
			'icon-twitter-circled' : '&#xe803;',
			'icon-facebook-circled' : '&#xe802;',
			'icon-gplus-circled' : '&#xe804;',
			'icon-pinterest-circled' : '&#xe801;',
			'icon-tumblr-circled' : '&#xe858;',
			'icon-linkedin-circled' : '&#xe857;',
			'icon-book-3' : '&#xe8aa;',
			'icon-mobile' : '&#xe88d;',
			'icon-arrow-right' : '&#xe005;',
			'icon-burger' : '&#xe006;',
			'icon-byerlys' : '&#xe007;',
			'icon-cub' : '&#xe008;',
			'icon-curts' : '&#xe009;',
			'icon-festival' : '&#xe00a;',
			'icon-hornbachers' : '&#xe00b;',
			'icon-jar' : '&#xe00c;',
			'icon-jar2' : '&#xe00d;',
			'icon-kowalskis' : '&#xe00e;',
			'icon-lunds' : '&#xe00f;',
			'icon-target' : '&#xe010;',
			'icon-arrow-left' : '&#xe011;'
		},
		els = document.getElementsByTagName('*'),
		i, attr, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		attr = el.getAttribute('data-icon');
		if (attr) {
			addIcon(el, attr);
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
};