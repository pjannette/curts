<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- META DATA -->
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
        <!--[if IE]><meta http-equiv="cleartype" content="on" /><![endif]-->

        <!-- SEO -->
        <title>Curt's Special Recipe</title>

        <!-- ICONS -->
        <link rel="shortcut icon" type="image/ico" href="assets/images/favicon.ico" />
        <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png" />
        <link rel="apple-touch-icon" href="assets/images/touch-icon-iphone.png" />
        <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-touch-icon-72x72-precomposed.png" />
        <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-touch-icon-114x114-precomposed.png" />
        <link rel="apple-touch-icon" sizes="144x144" href="assets/images/apple-touch-icon-144x144-precomposed.png" />

        <!--[if lt IE 9]>
            <link rel="stylesheet" type="text/css" href="assets/styles/ie8.css" />
            <script type="text/javascript" src="assets/scripts/html5shiv.js"></script>
        <![endif]-->

        <!-- STYLESHEETS -->
        <link rel="stylesheet" media="screen, projection" href="assets/css/screen.css" />
        <!-- <link rel="stylesheet" media="screen and (min-width: 480px)" href="assets/styles/screen_small.css" />
        <link rel="stylesheet" media="screen and (min-width: 800px)" href="assets/styles/screen_medium.css" />
        <link rel="stylesheet" media="screen and (min-width: 1024px)" href="assets/styles/screen_large.css" />
        <link rel="stylesheet" media="screen and (min-width: 1450px)" href="assets/styles/screen_huge.css" /> -->
        <!--[if lte IE 7]><script src="assets/scripts/lte-ie7.js"></script><![endif]-->

        <!-- JQUERY -->
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <script type="text/javascript">window.jQuery || document.write('<script type="text/javascript" src="assets/scripts/jquery.min.js"><\/script>')</script>

        <!-- TYPEKIT -->
        <script type="text/javascript" src="//use.typekit.net/odh1wpv.js"></script>
        <script type="text/javascript">try{Typekit.load();}catch(e){}</script>

        <!-- 3rd Party JS -->
        <script type="text/javascript" src="assets/scripts/respond.min.js"></script>
        <script type="text/javascript" src="assets/scripts/jquery.flexslider-min.js"></script>
        <script src="assets/scripts/modernizr.custom.js"></script>
        <script src="assets/scripts/classie.js"></script>
        <script src="assets/scripts/cbpScroller.js"></script>

        <!-- JAVASCRIPT -->
        <script type="text/javascript" src="assets/scripts/global.js"></script>

    </head>
    <body class="cbp-spmenu-push">
