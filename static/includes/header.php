    <!-- header -->
    <header id="siteHeader" class="globalHeader lTable" role="banner">
        <a href="index.php" class="globalLogo" title="Go to the homepage">
            <i class="icon icon_light icon-curts"></i>
            <h1 class="globalLogo-text">Curt's Special Recipe, Small Batch Sasla, Barbeque Sauce, and Bloody Mary Mix</h1>
        </a>

        <!-- REMOVE -->
        <div class="hdg hdg_1 hdg_onDark breakpointTester"></div>
        <!-- REMOVE -->     

        <!-- <div class="globalHeader-cart">
            <div class="icon icon-basket"></div> 
            <div class="">Cart (0)</div>
        </div> -->

        <nav class="globalNav cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="cbp-spmenu-s2">
        	<div class="isHidden_desktop globalNav-title"><i class="icon icon_light icon-curts"></i></div>
        	<ul class="globalNav-list">
        		<li><a href="index.php">HOME</a></li>
        		<li><a href="gallery.php">GALLERY</a></li>
        		<li><a href="wheretobuy.php">WHERE TO BUY</a></li>
        		<li><a href="contact.php">CONTACT</a></li>
        		<li><a href="shop.php">SHOP</a></li>
                <li class="globalHeader-cart">
                    <a href="#" class="snipcart-checkout snipcart-summary">
                        <span class="icon icon-basket globalHeader-cart_icon"></span>
                        <span class="globalHeader-cart_text">(<span class="snipcart-total-items"></span>)</span>
                    </a>
                </li>
        	</ul>
        </nav>
<!--         <span class="snipcart-summary">
    Number of items: 
    Total price: <span class="snipcart-total-price"></span>
    </span> -->
        <div id="js-showRightPush" class="mobileMenuIcon icon"></div>
    </header>

    
    <!-- end header -->

    <div class="toTop js-scrollTop">
        <i class="icon icon-jar"></i>
    </div>

    <main class="main" role="main">
